package luagamedev.editors;

import luagamedev.editors.model.Properties;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.FillLayout;

public class PropertiesEditDialog extends Dialog {

	protected Object result;
	protected Shell shlProperties;
	private PropertyEditor editor;
	private Properties props;
	private boolean dirty;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public PropertiesEditDialog(Shell parent, int style) {
		super(parent, SWT.DIALOG_TRIM);
		setText("SWT Dialog");
		dirty = false;
	}
	
	public boolean isDirty() {
		return dirty;
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shlProperties.open();
		shlProperties.layout();
		Display display = getParent().getDisplay();
		while (!shlProperties.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}
	
	public void setProperties(Properties props) {
		this.props = props;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlProperties = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shlProperties.setSize(450, 300);
		shlProperties.setText("Properties");
		shlProperties.setLayout(new FillLayout(SWT.HORIZONTAL));

		editor = new PropertyEditor(shlProperties, SWT.NULL);
		editor.setProperties(props);
		
		editor.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				dirty = true;
			}
		});
	}

}
