package luagamedev.editors.globalconf.model;

public class GlobalConfig {
	public static class GraphicsConfig {
		
		public static class WindowSize {
			public int width, height;
		}
		
		public String driver;
		public boolean vsync;
		public WindowSize window_size;
		
		public GraphicsConfig() {
			window_size = new WindowSize();
		}
	}
	
	public GraphicsConfig graphics;
	
	public GlobalConfig() {
		graphics = new GraphicsConfig();
	}
}
