package luagamedev.editors.globalconf;

import luagamedev.editors.BaseEditor;
import luagamedev.editors.globalconf.model.GlobalConfig;
import luagamedev.util.NumberVerify;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class GlobalConfEditor extends BaseEditor<GlobalConfig> {

	private Combo graphicsDriver;
	private Button vsync;
	private Text windowWidth, windowHeight;
	
	private void createGraphicsControl(Composite container) {
		Label label;
		RowLayout rowLayout;
		RowData rowData;
		
		rowLayout = new RowLayout();
		rowLayout.center = true;
		
		// 图形设置
		Group graphicsGroup = new Group(container, SWT.SHADOW_ETCHED_IN);
		graphicsGroup.setText("Graphics");
		
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		layout.verticalSpacing = 9;
		graphicsGroup.setLayout(layout);
		
		// 驱动选择
		Composite driverSelectComposite = new Composite(graphicsGroup, SWT.CENTER);
		driverSelectComposite.setLayout(rowLayout);
		
		label = new Label(driverSelectComposite, SWT.NULL);
		label.setText("Driver:");
		
		graphicsDriver = new Combo(driverSelectComposite, SWT.READ_ONLY);
		graphicsDriver.setItems(new String[] {
				"Direct3D",
				"OpenGL",
				"OpenGLES",
		});
		graphicsDriver.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				makeDirty();
			}
		});
		
		// 垂直同步
		vsync = new Button(graphicsGroup, SWT.CHECK);
		vsync.setText("VSync");
		vsync.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				makeDirty();
			}
		});
		
		// 窗口大小
		Composite windowSizeComposite = new Composite(graphicsGroup, SWT.CENTER);
		windowSizeComposite.setLayout(rowLayout);
		rowData = new RowData();
		rowData.width = 50;
		
		label = new Label(windowSizeComposite, SWT.NULL);
		label.setText("Width:");
		
		windowWidth = new Text(windowSizeComposite, SWT.BORDER | SWT.SINGLE);
		windowWidth.addListener(SWT.Verify, new NumberVerify(false));
		windowWidth.setLayoutData(rowData);
		windowWidth.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				makeDirty();
			}
		});

		label = new Label(windowSizeComposite, SWT.NULL);
		label.setText("Height:");
		
		windowHeight = new Text(windowSizeComposite, SWT.BORDER | SWT.SINGLE);
		windowHeight.addListener(SWT.Verify, new NumberVerify(false));
		windowHeight.setLayoutData(rowData);
		windowHeight.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				makeDirty();
			}
		});
	}

	@Override
	protected void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		
		layout.numColumns = 1;
		layout.verticalSpacing = 9;
		container.setLayout(layout);
		
		createGraphicsControl(container);
	}

	@Override
	protected void updateData(boolean toModel) {
		if (toModel) {
			model.graphics.driver = graphicsDriver.getText();
			model.graphics.vsync = vsync.getSelection();
			model.graphics.window_size.width = Integer.parseInt(windowWidth.getText());
			model.graphics.window_size.height = Integer.parseInt(windowHeight.getText());
		} else {
			graphicsDriver.select(graphicsDriver.indexOf(model.graphics.driver));
			vsync.setSelection(model.graphics.vsync);
			windowWidth.setText(String.format("%d", model.graphics.window_size.width));
			windowHeight.setText(String.format("%d", model.graphics.window_size.height));
		}
	}
}
