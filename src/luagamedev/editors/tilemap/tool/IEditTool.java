package luagamedev.editors.tilemap.tool;

import luagamedev.editors.tilemap.model.TileMapModel;
import luagamedev.editors.tilemap.ui.EditorMain;

import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;

public interface IEditTool {
	void handleMouseDown(Point pt);
	
	void handleMouseUp(Point pt);
	
	void handleMouseMove(Point pt);
}
