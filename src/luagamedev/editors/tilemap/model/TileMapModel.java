package luagamedev.editors.tilemap.model;

import luagamedev.editors.model.Properties;

public class TileMapModel {
	public static class Size {
		public int width, height;
	}
	
	public String type;
	
	public Size size, tilesize;
	
	public String[] tilesets;
	
	public Properties properties;
	
	public static class Layer {
		public String name;
		
		public int[] data;
		
		public Properties properties;
	}
	
	public Layer[] layers;
	
	public void addLayer(String name) {
		Layer[] newlayers = new Layer[layers.length + 1];
		System.arraycopy(layers, 0, newlayers, 0, layers.length);
		
		Layer newlayer = new Layer();
		
		newlayer.name = name;
		newlayer.data = new int[size.width * size.height];
		newlayers[layers.length] = newlayer;
		layers = newlayers;
	}
	
	public void deleteLayer(int i) {
		if (i >= 0 && i < layers.length) {
			Layer[] newlayers = new Layer[layers.length - 1];
			System.arraycopy(layers, 0, newlayers, 0, i);
			System.arraycopy(layers, i + 1, newlayers, i, layers.length - i - 1);
			layers = newlayers;
		}
	}
	
	public void layerToPrevious(int i) {
		if (i > 0 && i < layers.length) {
			Layer layer = layers[i];
			layers[i] = layers[i - 1];
			layers[i - 1] = layer;
		}
	}
	
	public void layerToNext(int i) {
		if (i >= 0 && i < layers.length - 1) {
			Layer layer = layers[i];
			layers[i] = layers[i + 1];
			layers[i + 1] = layer;
		}
	}
	
	public void addTileset(String filename) {
		String[] newtilesets = new String[tilesets.length + 1];
		System.arraycopy(tilesets, 0, newtilesets, 0, tilesets.length);
		newtilesets[tilesets.length] = filename;
		tilesets = newtilesets;
	}
	
	public void deleteTileset(int idx, int sid, int len) {
		if (idx >= 0 && idx < tilesets.length) {
			int eid = sid + len;
			
			for (int i = 0; i < layers.length; i++) {
				int[] data = layers[i].data;
				for (int j = 0; j < data.length; j++) {
					if (data[j] >= sid && data[j] < eid) {
						data[j] = 0;
					} else if (data[j] > eid) {
						data[j] -= len;
					}
				}
			}

			String[] newtilesets = new String[tilesets.length - 1];
			System.arraycopy(tilesets, 0, newtilesets, 0, idx);
			System.arraycopy(tilesets, idx + 1, newtilesets, idx, tilesets.length - idx - 1);
			tilesets = newtilesets;
		}
	}
}
