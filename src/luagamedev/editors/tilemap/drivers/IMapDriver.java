package luagamedev.editors.tilemap.drivers;

import luagamedev.editors.tilemap.model.TileMapModel;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;

public interface IMapDriver {
	
	public interface ITileDrawer {
		void drawTile(GC gc, int x, int y, int id);
	}
	
	Point getPixelSize(TileMapModel model);
	
	Point getPixelCroodToGrid(TileMapModel model, int x, int y);
	
	Point getGridToPixelCrood(TileMapModel model, int x, int y);
	
	void drawLayer(GC gc, int mapWidth, int mapHeight, int tileWidth, int tileHeight, 
			int[] layerData, ITileDrawer tileDrawer);
}
