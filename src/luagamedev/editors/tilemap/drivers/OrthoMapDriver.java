package luagamedev.editors.tilemap.drivers;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;

import luagamedev.editors.tilemap.drivers.IMapDriver.ITileDrawer;
import luagamedev.editors.tilemap.model.TileMapModel;

public class OrthoMapDriver implements IMapDriver {
	@Override
	public Point getPixelSize(TileMapModel model) {
		return new Point(
				model.tilesize.width * model.size.width,
				model.tilesize.height * model.size.height);
	}

	@Override
	public Point getPixelCroodToGrid(TileMapModel model, int x, int y) {
		return new Point(x / model.tilesize.width, y / model.tilesize.height);
	}

	@Override
	public Point getGridToPixelCrood(TileMapModel model, int x, int y) {
		return new Point(x * model.tilesize.width, y * model.tilesize.height);
	}

	@Override
	public void drawLayer(GC gc, int mapWidth, int mapHeight, int tileWidth, int tileHeight, 
			int[] layerData, ITileDrawer tileDrawer) {
		for (int row = 0; row < mapHeight; row++) {
			for (int col = 0; col < mapWidth; col++) {
				int p = row * mapWidth + col;
				int id = layerData[p];
				if (id > 0) tileDrawer.drawTile(gc, col * tileWidth, row * tileHeight, id);
			}
		}
	}


}
