package luagamedev.editors.tilemap.drivers;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;

import luagamedev.editors.tilemap.drivers.IMapDriver.ITileDrawer;
import luagamedev.editors.tilemap.model.TileMapModel;

public class IsoMapDriver implements IMapDriver {

	@Override
	public Point getPixelSize(TileMapModel model) {
		return new Point(
				model.tilesize.width * model.size.width,
				model.tilesize.height * model.size.height);
	}

	@Override
	public Point getPixelCroodToGrid(TileMapModel model, int x, int y) {
		int tilex, tiley;
		x -= model.size.height * model.tilesize.width / 2;
		tilex = x / model.tilesize.width;
		tiley = y / model.tilesize.height;
		return new Point(
				tiley + tilex,
				tiley - tilex);
	}

	@Override
	public Point getGridToPixelCrood(TileMapModel model, int x, int y) {
		int originx = model.size.height * model.tilesize.width / 2;
		return new Point(
				(x - y) * model.tilesize.width / 2 + originx,
				(x + y) * model.tilesize.height / 2);
	}

	@Override
	public void drawLayer(GC gc, int mapWidth, int mapHeight, int tileWidth,
			int tileHeight, int[] layerData, ITileDrawer tileDrawer) {
		int halfWidth = tileWidth / 2, halfHeight = tileHeight / 2;

		for (int row = 0; row < mapHeight; row++) {
			int dx, dy;
			
			dx = (mapWidth * halfWidth - halfWidth) - row * halfWidth;
			dy = row * halfHeight;
			
			for (int col = 0; col < mapWidth; col++) {
				int p = row * mapWidth + col;
				int id = layerData[p];
				if (id > 0) tileDrawer.drawTile(gc, dx, dy, id);
				dx += halfWidth;
				dy += halfHeight;
			}
		}
	}
}
