package luagamedev.editors.tilemap;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ScrollBar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import luagamedev.editors.BaseEditor;
import luagamedev.editors.PropertiesEditDialog;
import luagamedev.editors.model.Properties;
import luagamedev.editors.tile.model.TileModel;
import luagamedev.editors.tilemap.drivers.IMapDriver;
import luagamedev.editors.tilemap.drivers.IsoMapDriver;
import luagamedev.editors.tilemap.drivers.OrthoMapDriver;
import luagamedev.editors.tilemap.model.TileMapModel;
import luagamedev.editors.tilemap.tool.IEditTool;
import luagamedev.editors.tilemap.ui.EditorMain;
import luagamedev.editors.tilemap.ui.LayerSelector;
import luagamedev.editors.tilemap.ui.TileSelector;
import luagamedev.editors.tilemap.ui.TileSelector.TileSet;

public class TileMapEditor extends BaseEditor<TileMapModel> implements IMapDriver.ITileDrawer {
	
	private EditorMain main; 
	
	private ArrayList<TileSelector.TileSet> tilesetArray = new ArrayList<TileSelector.TileSet>();
	
	private IMapDriver driver;
	
	private static class TileDrawInfo {
		public Image img;
		public Rectangle rt;
	}
	
	private HashMap<Integer, TileDrawInfo> tileDrawInfo = new HashMap<Integer, TileDrawInfo>();
	
	private Image imageCache;
	
	private IEditTool tool;
	
	private Point prevMousePt;
	
	private boolean mouseDown = false;
	
	private class DrawTool implements IEditTool {
		private void setTile(Point pt) {
			int tileId = main.tileSelector.getSelectionId();
			TileMapModel.Layer layer = main.layerSelector.getCurrentLayer();
			if (tileId != -1 && layer != null) {
				layer.data[pt.x + pt.y * model.size.width] = tileId;
				updateTile(pt.x, pt.y);
				makeDirty();
			}
		}
		
		@Override
		public void handleMouseDown(Point pt) {
			setTile(pt);
		}

		@Override
		public void handleMouseUp(Point pt) {
		}

		@Override
		public void handleMouseMove(Point pt) {
			setTile(pt);
		}		
	}
	
	private class EraseTool implements IEditTool {
		private void setTile(Point pt) {
			TileMapModel.Layer layer = main.layerSelector.getCurrentLayer();
			if (layer != null) {
				layer.data[pt.x + pt.y * model.size.width] = 0;
				updateTile(pt.x, pt.y);
				makeDirty();
			}
		}

		@Override
		public void handleMouseDown(Point pt) {
			setTile(pt);
		}

		@Override
		public void handleMouseUp(Point pt) {
		}

		@Override
		public void handleMouseMove(Point pt) {
			setTile(pt);
		}		
	}
	
	private class PaintTool implements IEditTool {
		
		private Stack<Point> stack = new Stack<Point>();

		@Override
		public void handleMouseDown(Point pt) {
			TileMapModel.Layer layer = main.layerSelector.getCurrentLayer();
			int tileId = main.tileSelector.getSelectionId();
			stack.clear();
 
			if (layer != null && tileId != -1) {
				stack.push(pt);
				int sid = layer.data[pt.x + pt.y * model.size.width];
				int[] data = layer.data;
				
				while(stack.size() > 0) {
					Point p = stack.pop();
					int off = p.x + p.y * model.size.width;
					data[off] = tileId;
					if (p.x > 0 && data[(p.x - 1) + p.y * model.size.width] == sid) stack.push(new Point(p.x - 1,p.y));
					if (p.y > 0 && data[p.x + (p.y - 1) * model.size.width] == sid) stack.push(new Point(p.x, p.y - 1));
					if (p.x < model.size.width - 1 && data[(p.x + 1) + p.y * model.size.width] == sid) stack.push(new Point(p.x + 1, p.y));
					if (p.y < model.size.height - 1 && data[p.x + (p.y + 1) * model.size.width] == sid) stack.push(new Point(p.x, p.y + 1));
				}

				rebuildMapCache();
				makeDirty();
			}
		}

		@Override
		public void handleMouseUp(Point pt) {
		}

		@Override
		public void handleMouseMove(Point pt) {
		}
		
	}
	
	private class RectangleTool implements IEditTool {
		
		private Point start;

		@Override
		public void handleMouseDown(Point pt) {
			start = pt;
			handleMouseMove(start);
		}

		@Override
		public void handleMouseUp(Point pt) {
		}

		@Override
		public void handleMouseMove(Point pt) {
			int minx, maxx, miny, maxy;
			TileMapModel.Layer layer = main.layerSelector.getCurrentLayer();
			int tileId = main.tileSelector.getSelectionId();
			
			if (layer != null && tileId != -1) {
				int[] data = layer.data;
				
				minx = Math.min(start.x, pt.x);
				miny = Math.min(start.y, pt.y);
				maxx = Math.max(start.x, pt.x);
				maxy = Math.max(start.y, pt.y);
				
				for (int row = miny; row <= maxy; row++) {
					for (int col = minx; col <= maxx; col++) {
						data[row * model.size.width + col] = tileId;
					}
				}
				
				rebuildMapCache();
				makeDirty();
			}
		}
		
	}
	
	public TileMapEditor() {
		tool = new DrawTool();
	}
	
	private Point mousePtToGridPt(MouseEvent e) {
		Point pt = driver.getPixelCroodToGrid(model, 
				e.x + main.canvas.getHorizontalBar().getSelection(), 
				e.y + main.canvas.getVerticalBar().getSelection());
		if (pt.x >= 0 && pt.x < model.size.width &&
			pt.y >= 0 && pt.y < model.size.height) return pt;
		else return null;
	}

	@Override
	protected void createControl(Composite parent) {
		main = new EditorMain(parent, SWT.NULL);
		
		main.tltmDraw.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				tool = new DrawTool();
			}
		});
		
		main.tltmErase.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				tool = new EraseTool();
			}
		});
		
		main.tltmPaint.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				tool = new PaintTool();
			}
		});
		
		main.tltmRectangle.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				tool = new RectangleTool();
			}
		});
		
		main.tltmProperties.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if (model.properties == null) model.properties = new Properties();
				PropertiesEditDialog dialog = new PropertiesEditDialog(getSite().getShell(), SWT.NULL);
				dialog.setProperties(model.properties);
				dialog.open();
				if (dialog.isDirty()) makeDirty();
			}
		});

		main.canvas.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				paintMapWithOffset(e);
			}
		});
		
		main.canvas.getHorizontalBar().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				main.canvas.redraw();
				setMiniViewport();
			}
		});
		
		main.canvas.getVerticalBar().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				main.canvas.redraw();
				setMiniViewport();
			}
		});
		
		main.canvas.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent e) {
				resizeScrollBars();
			}
		});
		
		main.canvas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				prevMousePt = mousePtToGridPt(e);
				mouseDown = true;
				tool.handleMouseDown(prevMousePt);
			}

			@Override
			public void mouseUp(MouseEvent e) {
				tool.handleMouseUp(mousePtToGridPt(e));
				mouseDown = false;
			}
		});
		
		main.canvas.addMouseMoveListener(new MouseMoveListener() {			
			@Override
			public void mouseMove(MouseEvent e) {
				if (mouseDown) {
					Point pt = mousePtToGridPt(e);
					if (!pt.equals(prevMousePt)) {
						tool.handleMouseMove(pt);
						prevMousePt = pt;
					}
				}
			}
		});

		main.miniMap.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				Point viewportLt = main.miniMap.getViewportLt();
				main.canvas.getHorizontalBar().setSelection(viewportLt.x);
				main.canvas.getVerticalBar().setSelection(viewportLt.y);
				main.canvas.redraw();
			}
		});
		
		main.tileSelector.addListener(TileSelector.addTilesetEvent, new Listener() {
			@Override
			public void handleEvent(Event event) {
				model.addTileset(tilesetArray.get(tilesetArray.size() - 1).filename);
				rebuildTileDrawInfo();
				rebuildMapCache();
				makeDirty();
			}
		});
		
		main.tileSelector.addListener(TileSelector.delTilesetEvent, new Listener() {
			@Override
			public void handleEvent(Event event) {
				int deleteIdx = (Integer)event.data;
				int id = 1;
				TileSet tileset;
				
				for (int i = 0; i < deleteIdx; i++) {
					tileset = tilesetArray.get(i);
					id += (tileset.getRows() + 1) * tileset.getColumns();
				}
				
				tileset = tilesetArray.get(deleteIdx);
				
				model.deleteTileset(deleteIdx, id, (tileset.getRows() + 1) * tileset.getColumns());
				tilesetArray.remove(deleteIdx);
				rebuildTileDrawInfo();
				rebuildMapCache();
				makeDirty();
			}
		});
	} 
	
	private void setMiniViewport() {
		Rectangle clientArea = main.canvas.getClientArea();
		Rectangle viewport = new Rectangle(
				main.canvas.getHorizontalBar().getSelection(), 
				main.canvas.getVerticalBar().getSelection(), 
				clientArea.width, clientArea.height);
		main.miniMap.setViewport(viewport);
	}
	
	private void resizeScrollBars() {
		Rectangle clientArea = main.canvas.getClientArea();
		ScrollBar bar = main.canvas.getHorizontalBar();
		Point psz = driver.getPixelSize(model);
		if (bar != null) {
			bar.setMaximum(psz.x);
			bar.setThumb(clientArea.width);
			bar.setPageIncrement(clientArea.width);
		}
		bar = main.canvas.getVerticalBar();
		if (bar != null) {
			bar.setMaximum(psz.y);
			bar.setThumb(clientArea.height);
			bar.setPageIncrement(clientArea.height);
		}
		setMiniViewport();
	}

	protected Gson createGson() {
		 GsonBuilder builder = new GsonBuilder();
		 builder.registerTypeAdapter(Properties.class, new Properties.Adapter());
		 return builder.create();
	}

	@Override
	protected void updateData(boolean toModel) {
		if (!toModel) {
			// 设置地图驱动
			if (model.type.compareToIgnoreCase("ortho") == 0) driver = new OrthoMapDriver();
			else if (model.type.compareToIgnoreCase("iso") == 0) driver = new IsoMapDriver();
			
			// 载入tiles
			for (int i = 0; i < model.tilesets.length; i++) {
				try {
					IFile rfile = (IFile)getEditorInput().getAdapter(IFile.class);
					IPath filePath = rfile.getLocation().makeAbsolute();
					
					// 载入tile信息
					TileSelector.TileSet tileset = new TileSelector.TileSet();
					filePath = filePath.removeLastSegments(1);
					filePath = filePath.append(model.tilesets[i]);
					FileInputStream tileFileInput;
					tileFileInput = new FileInputStream(filePath.toFile());
					tileset.model = createGson().fromJson(new InputStreamReader(tileFileInput), TileModel.class);
					tileset.filename = tileset.model.filename;
					tileFileInput.close();
					
					// 载入tile图片
					filePath = filePath.removeLastSegments(1);
					filePath = filePath.append(tileset.model.filename);
					tileset.img = new Image(Display.getCurrent(), filePath.toString());
					
					tilesetArray.add(tileset);
				} catch (Exception e) {
				}
			}
			
			// 生成图块映射信息
			rebuildTileDrawInfo();
					
			// 填充图层列表
			main.layerSelector.setModel(model);
			main.layerSelector.addListener(LayerSelector.showLayerEvent, new Listener() {
				@Override
				public void handleEvent(Event event) {
					rebuildMapCache();
				}
			});
			
			main.layerSelector.addListener(LayerSelector.addLayerEvent, new Listener() {
				@Override
				public void handleEvent(Event event) {
					makeDirty();
				}
			});
			
			main.layerSelector.addListener(LayerSelector.deleteLayerEvent, new Listener() {
				@Override
				public void handleEvent(Event event) {
					rebuildMapCache();
					makeDirty();
				}
			});
			
			main.layerSelector.addListener(LayerSelector.reorderLayerEvent, new Listener() {
				@Override
				public void handleEvent(Event event) {
					rebuildMapCache();
					makeDirty();
				}
			});

			// 载入图块
			main.tileSelector.setMapFile((IFile)getEditorInput().getAdapter(IFile.class));
			main.tileSelector.setTiles(tilesetArray);

			// 生成地图位图缓存
			rebuildMapCache();
		}
	}
	
	private void rebuildMapCache() {
		Point sz = driver.getPixelSize(model);
		imageCache = new Image(Display.getCurrent(), sz.x, sz.y);
		
		GC gc = new GC(imageCache);
		try {
			gc.setBackground(new Color(null, 127, 127, 127));
			gc.fillRectangle(0, 0, sz.x, sz.y);
			
			for (int i = 0; i < model.layers.length; i++) {
				TileMapModel.Layer layer = model.layers[i];
				if (main.layerSelector.isLayerShown(layer.name)) {
					driver.drawLayer(gc, model.size.width, model.size.height, 
							model.tilesize.width, model.tilesize.height, layer.data, this);
				}
			}
			
			main.canvas.redraw();
			main.miniMap.setImage(imageCache);
		} finally {
			gc.dispose();
		}
	}
	
	private void updateTile(int x, int y) {
		/*
		GC gc = new GC(imageCache);
		
		try {
			gc.setBackground(new Color(null, 127, 127, 127));
			gc.fillRectangle(
					x * model.tilesize.width, y * model.tilesize.height,
					model.tilesize.width, model.tilesize.height);
			
			for (int i = 0; i < model.layers.length; i++) {
				TileMapModel.Layer layer = model.layers[i];
				if (main.layerSelector.isLayerShown(layer.name)) {
					drawTile(gc, x * model.tilesize.width, y * model.tilesize.height, layer.data[x + y * model.size.width]);
				}
			}

			main.canvas.redraw();
			main.miniMap.setImage(imageCache);
		} finally {
			gc.dispose();
		}
		*/
		rebuildMapCache();
	}
	
	private void rebuildTileDrawInfo() {
		int id = 1;
		tileDrawInfo.clear();
		
		for (int i = 0; i < tilesetArray.size(); i++) {
			TileSelector.TileSet tileset = tilesetArray.get(i);
			int rows = tileset.getRows();
			int cols = tileset.getColumns();
			int sid = id;
			
			for (int row = 0; row < rows; row++) {
				for (int col = 0; col < cols; col++) {
					Rectangle rt = new Rectangle(
							(tileset.model.margin + (tileset.model.space + tileset.model.size.width) * col),
							(tileset.model.margin + (tileset.model.space + tileset.model.size.height) * row),
							tileset.model.size.width,
							tileset.model.size.height
							);
					TileDrawInfo drawInfo = new TileDrawInfo();
					drawInfo.img = tileset.img;
					drawInfo.rt = rt;
					tileDrawInfo.put(sid, drawInfo);
					sid++;
				}
			}
			
			id += (rows + 1) * cols;
		}
	}
	
	private void paintMapWithOffset(PaintEvent e) {
		GC gc = e.gc;
		gc.drawImage(imageCache, 
				-main.canvas.getHorizontalBar().getSelection(),
				-main.canvas.getVerticalBar().getSelection());
	}

	@Override
	public void drawTile(GC gc, int x, int y, int id) {
		TileDrawInfo drawInfo = tileDrawInfo.get(id);
		
		if (drawInfo != null) {
			gc.drawImage(
					drawInfo.img, 
					drawInfo.rt.x, 
					drawInfo.rt.y, 
					drawInfo.rt.width, 
					drawInfo.rt.height, 
					x, 
					y - (drawInfo.rt.height - model.tilesize.height), 
					drawInfo.rt.width, 
					drawInfo.rt.height);	
		}
	}

}
