package luagamedev.editors.tilemap.ui;

import java.io.InputStreamReader;
import java.util.ArrayList;

import luagamedev.editors.model.Properties;
import luagamedev.editors.tile.model.TileModel;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.dialogs.ResourceListSelectionDialog;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class TileSelector extends Group {
	
	public static final int addTilesetEvent = 1000;
	public static final int delTilesetEvent = 1000 + 1;
	
	private IFile mapFile;
	
	public static class TileSet {
		public TileModel model;
		public Image img;
		public String filename;
		
		public int getColumns() {
			int width = img.getBounds().width;
			int cols = (width - model.margin * 2 + model.space) / (model.size.width + model.space);
			return cols;
		}
		
		public int getRows() {
			int height = img.getBounds().height;
			int rows = (height - model.margin * 2 + model.space) / (model.size.height + model.space);
			return rows;
		}
	}
	
	private ArrayList<TileSet> tiles;
	
	private TabFolder tabFolder;
	
	private String selectedTileName;
	
	private Point selectedTile;
	
	private Color selectionColor;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public TileSelector(Composite parent, int style) {
		super(parent, style);
		selectionColor = new Color(null, 255, 0, 0);
		setText("Tiles");
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.verticalSpacing = 0;
		setLayout(gridLayout);
		
		tabFolder = new TabFolder(this, SWT.NONE);
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		ToolBar toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		ToolItem importTile = new ToolItem(toolBar, SWT.NONE);
		importTile.setText("Import");
		importTile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ResourceListSelectionDialog dlg = new ResourceListSelectionDialog(getShell(), mapFile.getProject(), 
						IResource.FILE);
				dlg.setTitle("Select tile file");
				dlg.open();
				Object[] result = dlg.getResult();
				if (result != null && result.length > 0) {
					IFile file = (IFile)dlg.getResult()[0];
					appendTile(file);
				}
			}
		});
		
		ToolItem deleteTile = new ToolItem(toolBar, SWT.NONE);
		deleteTile.setText("Delete");
		deleteTile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int i = tabFolder.getSelectionIndex();
				if (i != -1) {
					if (tiles.get(i).model.name.equals(selectedTileName)) {
						selectedTileName = null;
					}
					tabFolder.getItem(i).dispose();
					if (selectedTileName == null && tiles.size() > 0) {
						selectedTileName = tiles.get(0).model.name;
						selectedTile = new Point(0, 0);
					}
					Event evt = new Event();
					evt.data = Integer.valueOf(i);
					notifyListeners(delTilesetEvent, evt);
				}
			}
		});
	}
	
	private boolean isExists(String name) {
		for (int i = 0; i < tiles.size(); i++) {
			if (tiles.get(i).model.name.equalsIgnoreCase(name))
				return true;
		}
		return false;
	}

	private TileSet loadTileFile(IFile file) {
		try {
			GsonBuilder builder = new GsonBuilder();
			builder.registerTypeAdapter(Properties.class, new Properties.Adapter());
			Gson gson = builder.create();
			TileModel model = gson.fromJson(new InputStreamReader(file.getContents()), TileModel.class);
			if (model != null) {
				// 得到tile文件的相对路径
				IPath tileRelPath = file.getLocation().makeRelativeTo(mapFile.getLocation().removeLastSegments(1));
				// 得到image文件的绝对路径
				IPath imgAbsPath = file.getLocation().removeLastSegments(1).append(model.filename);
				
				Image img = new Image(Display.getCurrent(), imgAbsPath.toString());
				if (img != null) {
					TileSet ts = new TileSet();
					ts.img = img;
					ts.model = model;
					ts.filename = tileRelPath.toString();
					return ts;
				}
			}
		} catch (Exception e) {
		}
		
		return null;
	}
	
	private void appendTile(IFile file) {
		TileSet ts = loadTileFile(file);
		if (ts == null) {
			MessageDialog.openError(getShell(), "Error", String.format("'%s' is not a tile file!", 
					file.getFullPath().toString()));
		} else if (isExists(ts.model.name)) {
			MessageDialog.openError(getShell(), "Error", String.format("Tile '%s' is exists!", 
					ts.model.name));
		} else {
			tiles.add(ts);
			TabItem item = new TabItem(tabFolder, SWT.NULL);
			item.setControl(createTileCanvas(tabFolder, ts));
			item.setText(ts.model.name);
			notifyListeners(addTilesetEvent, null);
		}
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	public void setMapFile(IFile mapFile) {
		this.mapFile = mapFile;
	}
	
	public void setTiles(ArrayList<TileSet> tiles) {
		this.tiles = tiles;
		for (int i = 0; i < tiles.size(); i++) {
			TileSet tileset = tiles.get(i);
			TabItem item = new TabItem(tabFolder, SWT.NULL);
			item.setControl(createTileCanvas(tabFolder, tileset));
			item.setText(tileset.model.name);
		}
		if (tiles.size() > 0) {
			// 设置默认选择
			selectedTileName = tiles.get(0).model.name;
			selectedTile = new Point(0, 0);
		}
	}
	
	private void resizeScrollBars(Canvas canvas, TileSet tileset) {
		Rectangle clientArea = canvas.getClientArea();
		ScrollBar bar = canvas.getHorizontalBar();
		if (bar != null) {
			bar.setMaximum(tileset.img.getBounds().width);
			bar.setThumb(clientArea.width);
			bar.setPageIncrement(clientArea.width);
		}
		bar = canvas.getVerticalBar();
		if (bar != null) {
			bar.setMaximum(tileset.img.getBounds().height);
			bar.setThumb(clientArea.height);
			bar.setPageIncrement(clientArea.height);
		}
	}
	
	private void clearSelected() {
		if (selectedTileName != null) {
			Canvas canvas = getCanvas(selectedTileName);
			selectedTileName = null;
			if (canvas != null) canvas.redraw();
		} else {
			selectedTileName = null;
		}
	}
	
	private Canvas getCanvas(String name) {
		for (int i = 0; i < tabFolder.getItemCount(); i++) {
			if (tabFolder.getItem(i).getText().equals(name)) {
				return (Canvas)tabFolder.getItem(i).getControl();
			}
		}
		
		return null;
	}
	
	private Canvas createTileCanvas(Composite parent, final TileSet tileset) {
		final Canvas canvas = new Canvas(parent, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.DOUBLE_BUFFERED);
		
		canvas.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent e) {
				resizeScrollBars(canvas, tileset);
			}
		});
		
		canvas.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				GC gc = e.gc;
				gc.drawImage(tileset.img, 
						-canvas.getHorizontalBar().getSelection(),
						-canvas.getVerticalBar().getSelection());
				
				if (selectedTileName != null && selectedTileName.equals(tileset.model.name)) {
					Rectangle rt = tileset.model.gridRect(selectedTile);
					gc.setForeground(selectionColor);
					rt.x -= canvas.getHorizontalBar().getSelection();
					rt.y -= canvas.getVerticalBar().getSelection();
					gc.drawRectangle(rt);
					rt.x += 2; rt.y += 2; rt.width -= 4; rt.height -= 4;
					gc.drawRectangle(rt);
				}
			}
		});
		
		canvas.getHorizontalBar().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				canvas.redraw();
			}
		});
		
		canvas.getVerticalBar().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				canvas.redraw();
			}
		});
		
		canvas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				int cols = tileset.getColumns();
				int rows = tileset.getColumns();

				int x = (e.x + canvas.getHorizontalBar().getSelection() - tileset.model.margin) / (tileset.model.size.width + tileset.model.space);
				int y = (e.y + canvas.getVerticalBar().getSelection() - tileset.model.margin) / (tileset.model.size.height + tileset.model.space);
				if (x < 0 || x >= cols ||
					y < 0 || y >= rows) {
					clearSelected();
				} else {
					selectedTileName = tileset.model.name;
					selectedTile = new Point(x, y);
					canvas.redraw();
				}
			}
		});
		
		return canvas;
	}
	
	public int getSelectionId() {
		if (selectedTileName != null) {
			int id = 1;
			
			for (int i = 0; i < tiles.size(); i++) {
				TileSet tileset = tiles.get(i);
				
				if (tileset.model.name.equals(selectedTileName)) {
					return id + selectedTile.x + selectedTile.y * tileset.getColumns();
				} else {
					id += (tileset.getRows() + 1) * tileset.getColumns();
				}
			}
		}
		
		return -1;
	}
}