package luagamedev.editors.tilemap.ui;


import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.Transform;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Canvas;

public class MiniMap extends Composite {
	
	public Canvas canvas;
	
	private Image img;
	
	private final float scale = 0.2f;
	
	private Rectangle viewport;
	
	private boolean mouseDown = false;
	
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public MiniMap(Composite parent, int style) {
		super(parent, style);
		setLayout(new FillLayout(SWT.HORIZONTAL));
		
		canvas = new Canvas(this, SWT.DOUBLE_BUFFERED | SWT.H_SCROLL | SWT.V_SCROLL);
		canvas.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				if (img != null) {
					GC gc = e.gc;
					Transform transform = new Transform(null, scale, 0, 0, scale, 
							-canvas.getHorizontalBar().getSelection(),
							-canvas.getVerticalBar().getSelection());
					gc.setTransform(transform);
					gc.drawImage(img, 0, 0);
					
					gc.setTransform(transform);
					gc.setLineWidth(10);
					gc.setForeground(new Color(null, 255, 0, 0));
					gc.drawRectangle(viewport);
				}
			}
		});
		
		canvas.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent e) {
				resizeScrollBars();
			}
		});
		
		canvas.getHorizontalBar().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				canvas.redraw();
			}
		});
		
		canvas.getVerticalBar().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				canvas.redraw();
			}
		});
		
		canvas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				setViewOffset(e.x, e.y);
				makeViewportVisiable();
				mouseDown = true;
				canvas.setCapture(true);
			}
			
			@Override
			public void mouseUp(MouseEvent e) {
				mouseDown = false;
				canvas.setCapture(false);
			}
		});
		
		canvas.addMouseMoveListener(new MouseMoveListener() {
			@Override
			public void mouseMove(MouseEvent e) {
				if (mouseDown) {
					setViewOffset(e.x,e.y);
					makeViewportVisiable();
				}
			}
		});
	}
	
	private void setViewOffset(int mx, int my) {
		int x = (int)((canvas.getHorizontalBar().getSelection() + mx) / scale);
		int y = (int)((canvas.getVerticalBar().getSelection() + my) / scale);
		viewport.x = x - viewport.width / 2;
		viewport.y = y - viewport.width / 2;
		
		if (viewport.x < 0) viewport.x = 0;
		if (viewport.y < 0) viewport.y = 0;
		if (viewport.x + viewport.width >= img.getBounds().width) viewport.x = img.getBounds().width - viewport.width;
		if (viewport.y + viewport.height >= img.getBounds().height) viewport.y = img.getBounds().height - viewport.height;
		
		makeViewportVisiable();
		mouseDown = true;
		canvas.setCapture(true);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	public void setImage(Image img) {
		this.img = img;
		canvas.redraw();
		resizeScrollBars();
	}
	
	public void setViewport(Rectangle viewport) {
		this.viewport = viewport;
		makeViewportVisiable();
		canvas.redraw();
	}
	
	private void resizeScrollBars() {
		if (img != null) {
			Rectangle clientArea = canvas.getClientArea();
			ScrollBar bar = canvas.getHorizontalBar();
			Point psz = new Point((int)(img.getBounds().width * scale), (int)(img.getBounds().height * scale));
			if (bar != null) {
				bar.setMaximum(psz.x);
				bar.setThumb(clientArea.width);
				bar.setPageIncrement(clientArea.width);
			}
			bar = canvas.getVerticalBar();
			if (bar != null) {
				bar.setMaximum(psz.y);
				bar.setThumb(clientArea.height);
				bar.setPageIncrement(clientArea.height);
			}
		}
	}
	
	private void makeViewportVisiable() {
		Rectangle rv = new Rectangle(
				(int)(viewport.x * scale),
				(int)(viewport.y * scale),
				(int)(viewport.width * scale),
				(int)(viewport.height * scale));
		if (rv.x < canvas.getHorizontalBar().getSelection()) {
			canvas.getHorizontalBar().setSelection(rv.x);
			canvas.redraw();
		}

		if (rv.y < canvas.getVerticalBar().getSelection()) {
			canvas.getVerticalBar().setSelection(rv.y);
			canvas.redraw();
		}
		
		if (rv.x + rv.width >= canvas.getHorizontalBar().getSelection() + canvas.getHorizontalBar().getSelection()) {
			int v = canvas.getHorizontalBar().getSelection() + canvas.getHorizontalBar().getThumb() - (rv.x + rv.width);
			canvas.getHorizontalBar().setSelection(canvas.getHorizontalBar().getSelection() - v);
			canvas.redraw();
		}

		if (rv.y + rv.height >= canvas.getVerticalBar().getSelection() + canvas.getVerticalBar().getThumb()) {
			int v = canvas.getVerticalBar().getSelection() + canvas.getVerticalBar().getThumb() - (rv.y + rv.height);
			canvas.getVerticalBar().setSelection(canvas.getVerticalBar().getSelection() - v);
			canvas.redraw();
		}
		
		notifyListeners(SWT.Selection, null);
	}
	
	public Point getViewportLt() {
		return new Point(viewport.x, viewport.y);
	}
}
