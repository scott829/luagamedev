package luagamedev.editors.tilemap.ui;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class EditorMain extends Composite {
	public LayerSelector layerSelector;
	public TileSelector tileSelector;
	public TabFolder tabFolder;
	private TabItem tbtmLayers;
	public MiniMap miniMap;
	private TabItem tbtmMiniMap;
	public Canvas canvas;
	private ToolBar toolBar;
	public ToolItem tltmDraw;
	public ToolItem tltmErase;
	private ToolItem toolItem;
	public ToolItem tltmProperties;
	public ToolItem tltmPaint;
	public ToolItem tltmRectangle;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public EditorMain(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(2, false));
		
		toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		tltmDraw = new ToolItem(toolBar, SWT.RADIO);
		tltmDraw.setText("Draw");
		tltmDraw.setSelection(true);
		
		tltmErase = new ToolItem(toolBar, SWT.RADIO);
		tltmErase.setText("Erase");
		
		tltmPaint = new ToolItem(toolBar, SWT.RADIO);
		tltmPaint.setText("Paint");
		
		tltmRectangle = new ToolItem(toolBar, SWT.NONE);
		tltmRectangle.setText("Rectangle");
		
		toolItem = new ToolItem(toolBar, SWT.SEPARATOR);
		
		tltmProperties = new ToolItem(toolBar, SWT.NONE);
		tltmProperties.setText("Properties");
		new Label(this, SWT.NONE);
		
		canvas = new Canvas(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.DOUBLE_BUFFERED);
		canvas.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));
		GridData gd_composite = new GridData(SWT.LEFT, SWT.FILL, false, true, 1, 1);
		gd_composite.widthHint = 300;
		composite.setLayoutData(gd_composite);
		
		tabFolder = new TabFolder(composite, SWT.NONE);
		GridData gd_tabFolder = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_tabFolder.heightHint = 250;
		tabFolder.setLayoutData(gd_tabFolder);
		
		tbtmMiniMap = new TabItem(tabFolder, SWT.NONE);
		tbtmMiniMap.setText("Mini Map");
		
		miniMap = new MiniMap(tabFolder, SWT.NULL);
		tbtmMiniMap.setControl(miniMap);
		
		tbtmLayers = new TabItem(tabFolder, SWT.NONE);
		tbtmLayers.setText("Layers");
		
		layerSelector = new LayerSelector(tabFolder, SWT.NONE);
		tbtmLayers.setControl(layerSelector);

		tileSelector = new TileSelector(composite, SWT.NULL);
		tileSelector.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
