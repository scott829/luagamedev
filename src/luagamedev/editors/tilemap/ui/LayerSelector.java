package luagamedev.editors.tilemap.ui;


import luagamedev.editors.PropertiesEditDialog;
import luagamedev.editors.model.Properties;
import luagamedev.editors.tilemap.model.TileMapModel;
import luagamedev.editors.tilemap.model.TileMapModel.Layer;

import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.wb.swt.ResourceManager;

public class LayerSelector extends Composite {
	
	public static final int addLayerEvent = 1000;
	public static final int showLayerEvent = 1000 + 1;
	public static final int deleteLayerEvent = 1000 + 2;
	public static final int reorderLayerEvent = 1000 + 3;
	public static final int setPropEvent = 1000 + 4;
	
	private TileMapModel model;
	private Table layerList;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public LayerSelector(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		layerList = new Table(this, SWT.BORDER | SWT.CHECK);
		layerList.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		TableColumn tblclmnNewColumn = new TableColumn(layerList, SWT.NONE);
		tblclmnNewColumn.setWidth(400);
		tblclmnNewColumn.setText("Name");
		
		ToolBar toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		ToolItem newLayer = new ToolItem(toolBar, SWT.NONE);
		newLayer.setText("New");
		newLayer.setToolTipText("New Layer");
		newLayer.setImage(ResourceManager.getPluginImage("LuaGameDev", "icons/sample.gif"));
		newLayer.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				// �½�ͼ��
				InputDialog inputDlg = new InputDialog(getShell(), "Create new layer", "Please input name of new layer:", "NewLayer", new IInputValidator() {
					@Override
					public String isValid(String newText) {
						for (int i = 0; i < model.layers.length; i++) {
							if (model.layers[i].name.compareToIgnoreCase(newText) == 0) {
								return String.format("Layer '%s' is exists", newText);
							}
						}
						return null;
					}
				});
				
				if (inputDlg.open() == InputDialog.OK) {
					TableItem item = new TableItem(layerList, SWT.NULL);
					item.setText(inputDlg.getValue());
					item.setChecked(true);
					layerList.select(layerList.getItemCount() - 1);
					model.addLayer(inputDlg.getValue());
					notifyListeners(addLayerEvent, null);
				}
			}
		});
		
		ToolItem toolItem_1 = new ToolItem(toolBar, SWT.SEPARATOR);
		
		ToolItem toPrevious = new ToolItem(toolBar, SWT.NONE);
		toPrevious.setImage(ResourceManager.getPluginImage("LuaGameDev", "icons/sample.gif"));
		toPrevious.setToolTipText("To Previous");
		toPrevious.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int i = layerList.getSelectionIndex();
				if (i > 0 && i < layerList.getItemCount()) {
					String name = layerList.getItem(i).getText();
					boolean show = layerList.getItem(i).getChecked();
					
					layerList.getItem(i).setText(layerList.getItem(i - 1).getText());
					layerList.getItem(i).setChecked(layerList.getItem(i - 1).getChecked());
					layerList.getItem(i - 1).setText(name);
					layerList.getItem(i - 1).setChecked(show);
					model.layerToPrevious(i);
					layerList.select(i - 1);
					notifyListeners(reorderLayerEvent, null);
				}
			}
		});
		
		ToolItem toNext = new ToolItem(toolBar, SWT.NONE);
		toNext.setImage(ResourceManager.getPluginImage("LuaGameDev", "icons/sample.gif"));
		toNext.setToolTipText("To Next");
		toNext.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int i = layerList.getSelectionIndex();
				if (i >= 0 && i < layerList.getItemCount() - 1) {
					String name = layerList.getItem(i).getText();
					boolean show = layerList.getItem(i).getChecked();
					
					layerList.getItem(i).setText(layerList.getItem(i + 1).getText());
					layerList.getItem(i).setChecked(layerList.getItem(i + 1).getChecked());
					layerList.getItem(i + 1).setText(name);
					layerList.getItem(i + 1).setChecked(show);
					model.layerToNext(i);
					layerList.select(i + 1);
					notifyListeners(reorderLayerEvent, null);
				}
			}
		});
		
		ToolItem shOthers = new ToolItem(toolBar, SWT.NONE);
		shOthers.setToolTipText("Show/Hide Other Layers");
		shOthers.setImage(ResourceManager.getPluginImage("LuaGameDev", "icons/sample.gif"));
		shOthers.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				boolean show = true;
				int i;
				
				for (i = 0; i < layerList.getItemCount(); i++) {
					if (layerList.getSelectionIndex() != i) {
						if (layerList.getItem(i).getChecked()) {
							show = false;
							break;
						}
					}
				}
				
				for (i = 0; i < layerList.getItemCount(); i++) {
					if (layerList.getSelectionIndex() != i) {
						layerList.getItem(i).setChecked(show);
					}
				}
				
				notifyListeners(showLayerEvent, null);
			}
		});
		
		ToolItem toolItem_2 = new ToolItem(toolBar, SWT.SEPARATOR);
		
		ToolItem deleteLayer = new ToolItem(toolBar, SWT.NONE);
		deleteLayer.setToolTipText("Delete");
		deleteLayer.setImage(ResourceManager.getPluginImage("LuaGameDev", "icons/sample.gif"));
		
		ToolItem toolItem = new ToolItem(toolBar, SWT.SEPARATOR);
		
		ToolItem tltmProperties = new ToolItem(toolBar, SWT.NONE);
		tltmProperties.setText("Properties");
		tltmProperties.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				PropertiesEditDialog dialog = new PropertiesEditDialog(getShell(), SWT.NULL);
				TileMapModel.Layer layer = getCurrentLayer();
				if (layer.properties == null) layer.properties = new Properties();
				dialog.setProperties(layer.properties);
				dialog.open();
				if (dialog.isDirty()) notifyListeners(setPropEvent, null);
			}
		});
		
		deleteLayer.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if (layerList.getSelectionIndex() >= 0) {
					model.deleteLayer(layerList.getSelectionIndex());
					layerList.remove(layerList.getSelectionIndex());
					layerList.select(0);
					notifyListeners(deleteLayerEvent, null);
				}
			}
		});

		layerList.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if ((e.detail & SWT.CHECK) != 0) {
					notifyListeners(showLayerEvent, null);
				}
			}
		});
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	public void setModel(TileMapModel model) {
		this.model = model;
		for (int i = 0; i < model.layers.length; i++) {
			TileMapModel.Layer layer = model.layers[i];
			TableItem item = new TableItem(layerList, SWT.NULL);
			item.setText(layer.name);
			item.setChecked(true);
		}
		layerList.select(0);
	}
	
	public boolean isLayerShown(String name) {
		for (int i = 0; i < layerList.getItemCount(); i++) {
			if (layerList.getItem(i).getText().equals(name)) {
				return layerList.getItem(i).getChecked();
			}
		}
		
		return false;
	}
	
	public TileMapModel.Layer getCurrentLayer() {
		if (layerList.getSelectionIndex() != -1) {
			return model.layers[layerList.getSelectionIndex()];
		} else {
			return null;
		}
	}
}
