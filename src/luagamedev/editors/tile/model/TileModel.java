package luagamedev.editors.tile.model;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;

import luagamedev.editors.model.Properties;

public class TileModel {
	public static class TileSize {
		public int width, height;
	}
		
	public static class Tile {
		public int id;
		public String alias;
		public Properties properties;
		
		public boolean isEmpty() {
			return (alias == null || alias.length() == 0) &&
					(properties == null || properties.size() == 0); 
		}
	}

	public String name, filename;
	public TileSize size = new TileSize();
	public int margin, space;
	public Tile[] tiles = new Tile[0];
	
	public TileModel.Tile createTile(int id) {
		TileModel.Tile tile;
		tile = getTile(id);
		if (tile != null) return tile;
		Tile[] newtiles = new Tile[tiles.length + 1];
		System.arraycopy(tiles, 0, newtiles, 0, tiles.length);
		tile = new Tile();
		tile.id = id;
		newtiles[tiles.length] = tile;
		tiles = newtiles;
		return tile;
	}
	
	public TileModel.Tile getTile(int id) {
		for (int i = 0; i < tiles.length; i++) {
			if (tiles[i].id == id) {
				return tiles[i];
			}
		}
		
		return null;
	}
	
	public Rectangle gridRect(Point pt) {
		Rectangle rt = new Rectangle(0, 0, 0, 0);
		rt.x = margin + pt.x * (size.width + space);
		rt.y = margin + pt.y * (size.height + space);
		rt.width = size.width;
		rt.height = size.height;
		return rt;
	}
}
