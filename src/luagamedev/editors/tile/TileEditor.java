package luagamedev.editors.tile;

import java.util.LinkedList;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Text;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import luagamedev.editors.BaseEditor;
import luagamedev.editors.PropertyEditor;
import luagamedev.editors.model.Properties;
import luagamedev.editors.tile.model.TileModel;

public class TileEditor extends BaseEditor<TileModel> {
	
	private Canvas canvas;
	private Text tileIdText, tileAliasText;
	private Image img;
	private int offsetX, offsetY;
	private Point selection;
	private Color selectionColor;
	private boolean mouseDown;
	private PropertyEditor propertyEditor;
	
	public TileEditor() {
		offsetX = offsetY = 0;
		selection = null;
		selectionColor = new Color(null, 255, 0, 0);
		mouseDown = false;
	}

	protected Gson createGson() {
		 GsonBuilder builder = new GsonBuilder();
		 builder.registerTypeAdapter(Properties.class, new Properties.Adapter());
		 return builder.create();
	}
	
	private void createBaseControl(Composite parent) {
		Label labelId, labelAlias;
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		layout.verticalSpacing = 9;
		parent.setLayout(layout);
		
		labelId = new Label(parent, SWT.NULL);
		labelId.setText("Id:");
		
		tileIdText = new Text(parent, SWT.BORDER | SWT.READ_ONLY);
		tileIdText.setLayoutData(new GridData(100, -1));
		
		labelAlias = new Label(parent, SWT.NULL);
		labelAlias.setText("Alias:");

		tileAliasText = new Text(parent, SWT.BORDER);
		tileAliasText.setLayoutData(new GridData(200, -1));
		tileAliasText.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == 0xd) {
					saveAlias();
				}
			}
		});
	}
	
	private Composite createTileConfigControl(Composite parent) {
		Composite container = new Composite(parent, SWT.BORDER);
		GridLayout containerLayout = new GridLayout();
		containerLayout.numColumns = 2;
		containerLayout.horizontalSpacing = 9;
		container.setLayout(containerLayout);
		
		Group groupBase;
		
		groupBase = new Group(container, SWT.NULL);
		groupBase.setText("Info");
		createBaseControl(groupBase);
		groupBase.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
		
		propertyEditor = new PropertyEditor(container, SWT.NULL);
		groupBase.setLayoutData(new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL));
		propertyEditor.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (event.type == SWT.Selection) {
					makeDirty();
				}
			}
		});
		return container;
	}
	
	private void paintCanvas(PaintEvent e) {
		GC gc = e.gc;
		gc.drawImage(img, -offsetX, -offsetY);
		
		if (selection != null) {
			Rectangle rt = model.gridRect(selection);
			rt.x -= offsetX;
			rt.y -= offsetY;
			gc.setForeground(selectionColor);
			gc.drawRectangle(rt);
			rt.x += 2; rt.y += 2; rt.width -= 4; rt.height -= 4;
			gc.drawRectangle(rt);
		}
	}

	@Override
	protected void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		
		layout.numColumns = 1;
		layout.verticalSpacing = 9;
		container.setLayout(layout);

		canvas = new Canvas(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.DOUBLE_BUFFERED);
		canvas.setLayoutData(new GridData(GridData.FILL_VERTICAL | GridData.FILL_HORIZONTAL));
		
		canvas.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				paintCanvas(e);
			}
		});
		
		canvas.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent e) {
				resizeScrollBars();
			}
		});
		
		canvas.getHorizontalBar().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				offsetX = ((ScrollBar)e.widget).getSelection();
				canvas.redraw();
			}
		});
		
		canvas.getVerticalBar().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				offsetY = ((ScrollBar)e.widget).getSelection();
				canvas.redraw();
			}
		});
		
		canvas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				saveAlias();
				selection = clientToGrid(new Point(e.x, e.y));
				canvas.redraw();
				mouseDown = true;
				canvas.setCapture(true);
				selectChanged();
			}
			
			@Override
			public void mouseUp(MouseEvent e) {
				canvas.setCapture(false);
				mouseDown = false;
			}
		});
		
		canvas.addMouseMoveListener(new MouseMoveListener() {
			@Override
			public void mouseMove(MouseEvent e) {
				if (mouseDown) {
					selection = clientToGrid(new Point(e.x, e.y));
					canvas.redraw();
					canvas.setCapture(true);
					selectChanged();
				}
			}
		});

		Composite tileConfig = createTileConfigControl(container);
		GridData bottomLayoutData = new GridData(SWT.FILL, SWT.CENTER, true, false);
		tileConfig.setLayoutData(bottomLayoutData);
		
		enableEdit(false);
	}
	
	@Override
	protected void updateData(boolean toModel) {
		if (!toModel) {
			IFile rfile = (IFile)getEditorInput().getAdapter(IFile.class);
			IPath filePath = rfile.getLocation().makeAbsolute();
			filePath = filePath.removeLastSegments(1);
			filePath = filePath.append(model.filename);
			img = new Image(Display.getCurrent(), filePath.toString());
			canvas.setSize(img.getBounds().width, img.getBounds().height);
		}
	}
	
	private void resizeScrollBars() {
		Rectangle clientArea = canvas.getClientArea();
		ScrollBar bar = canvas.getHorizontalBar();
		if (bar != null) {
			bar.setMaximum(img.getBounds().width);
			bar.setThumb(clientArea.width);
			bar.setPageIncrement(clientArea.width);
		}
		bar = canvas.getVerticalBar();
		if (bar != null) {
			bar.setMaximum(img.getBounds().height);
			bar.setThumb(clientArea.height);
			bar.setPageIncrement(clientArea.height);
		}
	}
	
	private int getTileColumnCount() {
		int width = img.getBounds().width;
		return (width - model.margin * 2 + model.space) / (model.size.width + model.space);
	}

	private int getTileRowCount() {
		int height = img.getBounds().height;
		return (height - model.margin * 2 + model.space) / (model.size.height + model.space);
	}
	
	private Point clientToGrid(Point pt) {
		int x = (pt.x + offsetX - model.margin) / (model.size.width + model.space);
		int y = (pt.y + offsetY - model.margin) / (model.size.height + model.space);
		if (x < 0) return null;
		if (x >= getTileColumnCount()) return null;
		if (y < 0) return null;
		if (y >= getTileRowCount()) return null;
		return new Point(x, y);
	}
	
	private void enableEdit(boolean enable) {
		propertyEditor.setEnabled(enable);
		tileAliasText.setEnabled(enable);
	}
	
	private int getSelectionId() {
		if (selection != null) {
			int id = selection.x + selection.y * getTileColumnCount() + 1;
			if (id < 0 || id >= getTileRowCount() * getTileColumnCount()) return -1;
			return id;
		} else return -1;
	}
	
	private void selectChanged() {
		tileIdText.setText("");

		if (getSelectionId() != -1) {
			tileIdText.setText(String.format("%d", getSelectionId()));
			tileIdText.setEnabled(true);
			enableEdit(true);
			
			TileModel.Tile tile = model.createTile(getSelectionId());
			if (tile.alias != null) tileAliasText.setText(tile.alias);
			else tileAliasText.setText("");
			
			if (tile.properties == null) tile.properties = new Properties();
			propertyEditor.setProperties(tile.properties);

		} else {
			tileIdText.setEnabled(false);
			tileIdText.setText("");
			enableEdit(false);
		}
	}
	
	protected void beforeSave() {
		// 删除不必要的tile对象
		LinkedList<TileModel.Tile> newtiles = new LinkedList<TileModel.Tile>();
		for (int i = 0; i < model.tiles.length; i++) {
			if (model.tiles[i].alias != null && model.tiles[i].alias.length() == 0)
				model.tiles[i].alias = null;
			if (!model.tiles[i].isEmpty()) {
				newtiles.add(model.tiles[i]);
			}
		}
		model.tiles = newtiles.toArray(new TileModel.Tile[newtiles.size()]);
	}
	
	private void saveAlias() {
		if (getSelectionId() > 0) {
			TileModel.Tile tile = model.createTile(getSelectionId());
			if (!tileAliasText.getText().equals(tile.alias == null ? "" : tile.alias)) {
				tile.alias = tileAliasText.getText();
				makeDirty();
			}
		}
	}
}
