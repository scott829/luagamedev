package luagamedev.editors;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.ParameterizedType;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

import com.google.gson.Gson;

public abstract class BaseEditor<T> extends EditorPart {

	private boolean dirty, loaded;
	protected T model;
	
	public BaseEditor() {
		dirty = false;
		loaded = false;
	}
	
	protected Gson createGson() {
		return new Gson();
	}
	
	@Override
	public void doSave(IProgressMonitor monitor) {
		try {
			updateData(true);
			beforeSave();
			Gson gson = createGson();
			String json = gson.toJson(model);
			IFile file = (IFile)getEditorInput().getAdapter(IFile.class);
			file.setContents(new ByteArrayInputStream(json.getBytes()), IFile.FORCE, null);
			dirty = false;
			firePropertyChange(PROP_DIRTY);
			firePropertyChange(PROP_DIRTY);
		} catch (CoreException e) {
			e.printStackTrace();
		}		
	}
	
	@Override
	public void doSaveAs() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		setSite(site);
		setInput(input);
		
		IFile rfile = (IFile)getEditorInput().getAdapter(IFile.class);
		
		try {
			T genType = (T)getClass().getGenericSuperclass();   
			T[] params = (T[])((ParameterizedType) genType).getActualTypeArguments();   
			Class<?> entityClass = (Class<?>)params[0];   
			
			Gson gson = createGson();
			model = (T)gson.fromJson(new InputStreamReader(rfile.getContents()), entityClass);
		} catch (Exception e) {
			throw new PartInitException(String.format("cannot load %s", rfile.getName()));
		}
	}
	
	@Override
	public boolean isDirty() {
		return dirty;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void createPartControl(Composite parent) {
		createControl(parent);
		updateData(false);
		loaded = true;
	}

	@Override
	public void setFocus() {
	}
	
	protected void makeDirty() {
		if (loaded) {
			this.dirty = true;
			firePropertyChange(PROP_DIRTY);
		}
	}
	
	protected abstract void createControl(Composite parent);
	
	protected abstract void updateData(boolean toModel);
	
	protected void beforeSave() {
		
	}
}
