package luagamedev.editors;

import luagamedev.editors.model.Property;
import luagamedev.util.NumberVerify;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class PropertyEditDialog extends TitleAreaDialog {
	
	private Text nameText, valueNumber, valueText;
	private Button valueBool;
	private Composite valueComposite;
	private Combo typeCombo;
	private Property prop; 
	private String name;

	public PropertyEditDialog(Shell parentShell, String name, Property prop) {
		super(parentShell);
		setHelpAvailable(false);
		this.prop = prop;
		this.name = name;
		setDialogHelpAvailable(false);
	}

	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Edit Property");
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		layout.verticalSpacing = 9;
		container.setLayout(layout);
		
		Label label = new Label(container, SWT.NULL);
		label.setText("&Name:");
		
		nameText = new Text(container, SWT.BORDER);
		nameText.setLayoutData(new GridData(100, -1));
		nameText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				if (nameText.getText().length() == 0) {
					setErrorMessage("Name cannot be empty!");
				} else {
					setErrorMessage(null);
				}
			}
		});
		
		label = new Label(container, SWT.NULL);
		label.setText("&Type:");
		
		typeCombo = new Combo(container,  SWT.BORDER | SWT.READ_ONLY);
		typeCombo.add("String");
		typeCombo.add("Number");
		typeCombo.add("Boolean");
		typeCombo.select(0);
		typeCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleSwitchValueType();
			}
		});
		
		label = new Label(container, SWT.NULL);
		label.setText("&Value:");
		
		valueComposite = new Composite(container, SWT.NULL);
		FillLayout fillLayout = new FillLayout();
		valueComposite.setLayout(fillLayout);
		handleSwitchValueType();
		valueComposite.setLayoutData(new GridData(200, -1));
		
		loadProp();
		
		setTitle("Modify property of tile");
		setMessage("set propery of tile value:");
		return container;
	}
	
	private void loadProp() {
		if (prop != null) {
			nameText.setText(name);
			if (prop.type == Property.Type.STRING) {
				typeCombo.select(0);
				valueText.setText(prop.stringValue);
			} else if (prop.type == Property.Type.NUMBER) {
				typeCombo.select(1);
				valueNumber.setText(prop.numberValue.toString());
			} else if (prop.type == Property.Type.BOOLEAN) {
				typeCombo.select(2);
				valueBool.setSelection(prop.boolValue);
			}
		}
	}
	
	private void handleSwitchValueType() {
		int type = typeCombo.getSelectionIndex();
		
		if (valueText != null) {
			valueText.dispose();
			valueText = null;
		}
		
		if (valueNumber != null) {
			valueNumber.dispose();
			valueNumber = null;
		}
		
		if (valueBool != null) {
			valueBool.dispose();
			valueBool = null;
		}
		
		switch(type) {
		case 0:
			valueText = new Text(valueComposite, SWT.BORDER);
			break;
		case 1:
			valueNumber = new Text(valueComposite, SWT.BORDER);
			valueNumber.addListener(SWT.Verify, new NumberVerify(true));
			break;
		case 2:
			valueBool = new Button(valueComposite, SWT.CHECK);
			break;
		}
		
		valueComposite.layout();
	}
	
	public String getName() {
		return name;
	}
	
	protected void okPressed() {
		if (prop == null) {
			prop = new Property();
		}
		
		name = nameText.getText();
		
		if (typeCombo.getSelectionIndex() == 0) {
			prop.type = Property.Type.STRING;
			prop.stringValue = valueText.getText();
		} else if (typeCombo.getSelectionIndex() == 1) {
			prop.type = Property.Type.NUMBER;
			prop.numberValue = Float.valueOf(valueNumber.getText());
		} else if (typeCombo.getSelectionIndex() == 2) {
			prop.type = Property.Type.BOOLEAN;
			prop.boolValue = valueBool.getSelection();
		}
		
		super.okPressed();
	}
	
	public Property getProperty() {
		return prop;
	}

}
