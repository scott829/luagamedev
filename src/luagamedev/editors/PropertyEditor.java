package luagamedev.editors;

import java.util.Iterator;

import luagamedev.editors.model.Properties;
import luagamedev.editors.model.Property;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.ISelectionListener;

public class PropertyEditor extends Composite {
	private Table propertiesTable;
	private Button removeBtn, editBtn;
	private Properties props;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public PropertyEditor(Composite parent, int style) {
		super(parent, style);
		setLayout(new FillLayout(SWT.HORIZONTAL));
		
		Group grpProperties = new Group(this, SWT.NONE);
		grpProperties.setText("Properties");
		GridLayout gl_grpProperties = new GridLayout(2, false);
		gl_grpProperties.verticalSpacing = 9;
		grpProperties.setLayout(gl_grpProperties);
		grpProperties.setBounds(0, 0, 70, 84);
		
		propertiesTable = new Table(grpProperties, SWT.BORDER | SWT.FULL_SELECTION);
		propertiesTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		propertiesTable.setHeaderVisible(true);
		propertiesTable.setLinesVisible(true);
		propertiesTable.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				selectChanged();
			}
		});

		TableColumn tblclmnNewColumn = new TableColumn(propertiesTable, SWT.NONE);
		tblclmnNewColumn.setWidth(100);
		tblclmnNewColumn.setText("Name");
		
		TableColumn tblclmnNewColumn_1 = new TableColumn(propertiesTable, SWT.NONE);
		tblclmnNewColumn_1.setWidth(300);
		tblclmnNewColumn_1.setText("Value");
		
		Composite composite = new Composite(grpProperties, SWT.NONE);
		GridData gd_composite = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
		gd_composite.widthHint = 70;
		composite.setLayoutData(gd_composite);
		GridLayout gl_composite = new GridLayout(1, false);
		gl_composite.marginWidth = 0;
		gl_composite.verticalSpacing = 0;
		composite.setLayout(gl_composite);
		
		Button addBtn = new Button(composite, SWT.NONE);
		addBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		addBtn.setText("&Add");
		addBtn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleAddProperty();
			}
		});

		editBtn = new Button(composite, SWT.NONE);
		editBtn.setEnabled(false);
		editBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		editBtn.setText("&Edit");
		editBtn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleEditProperty();
			}
		});

		removeBtn = new Button(composite, SWT.NONE);
		removeBtn.setEnabled(false);
		removeBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		removeBtn.setText("Remove");
		removeBtn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleRemoveProperty();
			}
		});
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	public void setProperties(Properties props) {
		this.props = props;
		updatePropertiesList();
		propertiesTable.select(-1);
		removeBtn.setEnabled(false);
		editBtn.setEnabled(false);
	}
	
	void handleAddProperty() {
		PropertyEditDialog dlg = new PropertyEditDialog(getShell(), null, null);
		if (dlg.open() == PropertyEditDialog.OK) {
			props.put(dlg.getName(), dlg.getProperty());
			updatePropertiesList();
			notifyListeners(SWT.Selection, null);
		}
	}

	void handleRemoveProperty() {
		if (propertiesTable.getSelectionCount() > 0) {
			String name = propertiesTable.getSelection()[0].getText(0);
			props.remove(name);
			updatePropertiesList();
			notifyListeners(SWT.Selection, null);
		}
	}

	void handleEditProperty() {
		if (propertiesTable.getSelectionCount() > 0) {
			String name = propertiesTable.getSelection()[0].getText(0);
			PropertyEditDialog dlg = new PropertyEditDialog(getShell(), 
					name, props.get(name));
			if (dlg.open() == PropertyEditDialog.OK) {
				if (!name.equals(dlg.getName())) props.remove(name);
				props.put(dlg.getName(), dlg.getProperty());				
				updatePropertiesList();
				notifyListeners(SWT.Selection, null);
			}
		}
	}
		
	private void updatePropertiesList() {
		propertiesTable.removeAll();
		
		Iterator<String> it = props.keySet().iterator();
		while(it.hasNext()) {
			String name = it.next();
			Property prop = props.get(name);
			
			TableItem itemName = new TableItem(propertiesTable, SWT.NULL);
			itemName.setText(name);
			itemName.setText(1, prop.toString());
		}
	}
	
	private void selectChanged() {
		boolean hasSelect = propertiesTable.getSelectionCount() > 0;
		editBtn.setEnabled(hasSelect);
		removeBtn.setEnabled(hasSelect);
	}
}
