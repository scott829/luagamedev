package luagamedev.editors.model;

import java.io.IOException;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

public class Property {
	public enum Type {
		STRING,
		NUMBER,
		BOOLEAN,
		NULL
	}

	public Type type;
	public String stringValue;
	public Number numberValue;
	public Boolean boolValue;
	
	public String toString() {
		if (type == Type.STRING) {
			return stringValue;
		} else if (type == Type.NUMBER) {
			return numberValue.toString();
		} else if (type == Type.BOOLEAN) {
			return boolValue.toString();
		} else {
			return "<unknown type>";
		}
	}
	
	public static class Adapter extends TypeAdapter<Property> {
		@Override
		public void write(JsonWriter out, Property value) throws IOException {
			if (value.type == Type.STRING) {
				out.value(value.stringValue);
			} else if (value.type == Type.NUMBER) {
				out.value(value.numberValue);
			} else if (value.type == Type.BOOLEAN) {
				out.value(value.boolValue);
			} else {
				out.nullValue();
			}
		}

		@Override
		public Property read(JsonReader in) throws IOException {
			if (in.peek() == JsonToken.STRING) {
				Property result = new Property();
				result.type = Type.STRING;
				result.stringValue = in.nextString();
				return result;
			} else if (in.peek() == JsonToken.NUMBER) {
				Property result = new Property();
				result.type = Type.NUMBER;
				result.numberValue = in.nextDouble();
				return result;
			} else if (in.peek() == JsonToken.BOOLEAN) {
				Property result = new Property();
				result.type = Type.BOOLEAN;
				result.boolValue = in.nextBoolean();
				return result;
			} else {
				in.skipValue();
				return null;
			}
		}
	}
}
