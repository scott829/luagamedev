package luagamedev.editors.model;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

public class Properties extends HashMap<String, Property>{
	public static class Adapter extends TypeAdapter<Properties> {
		
		private Property.Adapter propertyAdapter = new Property.Adapter();
		
		@Override
		public void write(JsonWriter out, Properties value) throws IOException {
			if (value != null && value.size() > 0) {
				out.beginObject();
				Iterator<Map.Entry<String, Property>> it = value.entrySet().iterator();
				while(it.hasNext()) {
					Map.Entry<String, Property> entry = it.next();
					out.name(entry.getKey());
					propertyAdapter.write(out, entry.getValue());
				}
				out.endObject();
			} else {
				out.nullValue();
			}
		}

		@Override
		public Properties read(JsonReader in) throws IOException {
			Properties props = new Properties();
			in.beginObject();
			while(in.peek() != JsonToken.END_OBJECT) {
				String name = in.nextName();
				Property prop = propertyAdapter.read(in);
				props.put(name, prop);
			}
			in.endObject();
			return props;
		}
	}
}
