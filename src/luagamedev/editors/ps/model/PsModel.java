package luagamedev.editors.ps.model;

public class PsModel {
	// 粒子属性
	public float emissionRate; // 每秒发射多少粒子
	public int maxParticles; // 最大粒子数量
	public float lifeSpan; // 决定例子会在几秒钟内从屏幕上消失
	public float lifeSpanVariance;
	public float startSize; // 起始大小
	public float startSizeVariance;
	public float finishSize; // 终止大小
	public float finishSizeVariance;
	public float emitAngle; // 发射角度
	public float emitAngleVariance;
	public float rotationStart; // 起始旋转角度
	public float rotationStartVariance;
	public float rotationEnd; // 终止旋转角度
	public float rotationEndVariance;

	// 粒子颜色
	public float startRed; // 起始红色
	public float startGreen; // 起始绿色
	public float startBlue; // 起始蓝色
	public float startAlpha; // 起始透明度
	public float startRedVariance;
	public float startGreenVariance;
	public float startBlueVariance;
	public float startAlphaVariance;

	public float finishRed; // 终止红色
	public float finishGreen; // 终止绿色
	public float finishBlue; // 终止蓝色
	public float finishAlpha; // 终止透明度
	public float finishRedVariance;
	public float finishGreenVariance;
	public float finishBlueVariance;
	public float finishAlphaVariance;
	
	public String texture;

	// 发射器通用属性
	public float duration; // 发射器持续时间
	public String type; // 发射器类型
	public float sourcePositionX; // 发射器X坐标
	public float sourcePositionXVariance;
	public float sourcePositionY; // 发射器Y坐标
	public float sourcePositionYVariance;

	// 重力模式属性
	public float speed; // 粒子的速度
	public float speedVariance;
	public float gravityX, gravityY; // 粒子的重力
	public float radialAccel; // 径向加速度
	public float radialAccelVariance;
	public float tangencialAccel; // 正切加速度
	public float tangencialAccelVariance;
	public boolean rotationIsDir;

	// 半径模式
	public float startRadius; // 开始时的半径
	public float startRadiusVariance;
	public float endRadius; // 结束时的半径
	public float endRadiusVariance;
	public float rotatePerSecond; // 粒子围绕原点每秒旋转的度数
	public float rotatePerSecondVariance;

}
