package luagamedev.editors.ps;

import luagamedev.util.NumberVerify;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.GridData;

public class ValueScaleHorz extends Composite {
	private Text text;
	private Scale scale;
	public final static int Changed = 1000 + 1;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public ValueScaleHorz(Composite parent, int style) {
		super(parent, style);
		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.horizontalSpacing = 0;
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;
		gridLayout.verticalSpacing = 0;
		setLayout(gridLayout);
		
		scale = new Scale(this, SWT.NONE);
		scale.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		
		text = new Text(this, SWT.BORDER);
		GridData gd_text = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_text.widthHint = 50;
		text.setLayoutData(gd_text);
		text.addListener(SWT.Verify, new NumberVerify(true));
		text.setText("0");
		
		scale.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				text.setText(Integer.toString(scale.getSelection()));
			}
		});
		
		scale.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				notifyListeners(Changed, null);
			}
		});
		
		text.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				notifyListeners(Changed, null);
			}
		});
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public float getValue() {
		return Float.valueOf(text.getText());
	}

	public void setRange(int min, int max) {
		scale.setMaximum(max);
		scale.setMaximum(min);
	}
	
	public void setValue(float value) {
		text.setText(Float.valueOf(value).toString());
		scale.setSelection((int)value);
	}

}
