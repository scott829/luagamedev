package luagamedev.editors.ps;

import luagamedev.editors.BaseEditor;
import luagamedev.editors.ps.model.PsModel;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.dialogs.ResourceListSelectionDialog;

public class PSEditor extends BaseEditor<PsModel> {
	
	private EditorMain main;
	private Image imgTexture;
	private String texturePath;

	@Override
	protected void createControl(Composite parent) {
		main = new EditorMain(parent, SWT.NULL);
		
		ValueScaleHorz[] scales = new ValueScaleHorz[] {
				main.emissionRate,
				main.maxParticles,
				main.lifeSpan,
				main.lifeSpanVariance,
				main.startSize,
				main.startSizeVariance,
				main.finishSize,
				main.finishSizeVariance,
				main.emitAngle,
				main.emitAngleVariance,
				main.rotationStart,
				main.rotationEnd,
				main.rotationEndVariance,
				
				main.startRed,
				main.startGreen,
				main.startBlue,
				main.startAlpha,
				main.startRedVariance,
				main.startGreenVariance,
				main.startBlueVariance,
				main.startAlphaVariance,

				main.finishRed,
				main.finishGreen,
				main.finishBlue,
				main.finishAlpha,
				main.finishRedVariance,
				main.finishGreenVariance,
				main.finishBlueVariance,
				main.finishAlphaVariance,

				main.duration,
				main.sourcePositionX,
				main.sourcePositionXVariance,
				main.sourcePositionY,
				main.sourcePositionYVariance,
				
				main.speed,
				main.speedVariance,
				main.gravityX,
				main.gravityY,
				main.radialAccel,
				main.radialAccelVariance,
				main.tangencialAccel,
				main.tangencialAccelVariance,
				
				main.startRadius,
				main.startRadiusVariance,
				main.endRadius,
				main.endRadiusVariance,
				main.rotatePerSecond,
				main.rotatePerSecondVariance
		};
		
		for (int i = 0; i < scales.length; i++) {
			scales[i].addListener(ValueScaleHorz.Changed, new Listener() {
				@Override
				public void handleEvent(Event event) {
					makeDirty();
				}
			});
		}
		
		main.emissionRate.setRange(0, 500);
		main.maxParticles.setRange(0, 2000);
		main.lifeSpan.setRange(0, 100);
		main.lifeSpanVariance.setRange(0, 100);
		main.startSize.setRange(0, 500);
		main.startSizeVariance.setRange(0, 500);
		main.finishSize.setRange(0, 500);
		main.finishSizeVariance.setRange(0, 500);
		main.emitAngle.setRange(0, 360);
		main.emitAngleVariance.setRange(0, 360);
		main.rotationStart.setRange(0, 360);
		main.rotationEnd.setRange(0, 360);
		main.rotationEndVariance.setRange(0, 360);

		main.startRed.setRange(0, 1);
		main.startGreen.setRange(0, 1);
		main.startBlue.setRange(0, 1);
		main.startAlpha.setRange(0, 1);

		main.startRedVariance.setRange(0, 1);
		main.startGreenVariance.setRange(0, 1);
		main.startBlueVariance.setRange(0, 1);
		main.startAlphaVariance.setRange(0, 1);

		main.finishRed.setRange(0, 1);
		main.finishGreen.setRange(0, 1);
		main.finishBlue.setRange(0, 1);
		main.finishAlpha.setRange(0, 1);

		main.finishRedVariance.setRange(0, 1);
		main.finishGreenVariance.setRange(0, 1);
		main.finishBlueVariance.setRange(0, 1);
		main.finishAlphaVariance.setRange(0, 1);
		
		main.duration.setRange(0, 1000);
		main.emitterType.select(0);
		main.sourcePositionX.setRange(0, 1000);
		main.sourcePositionXVariance.setRange(0, 1000);
		main.sourcePositionY.setRange(0, 1000);
		main.sourcePositionYVariance.setRange(0, 1000);
		
		main.speed.setRange(0, 1000);
		main.speedVariance.setRange(0, 1000);
		main.gravityX.setRange(0, 1000);
		main.gravityY.setRange(0, 1000);
		main.radialAccel.setRange(0, 1000);
		main.radialAccelVariance.setRange(0, 1000);
		main.tangencialAccel.setRange(0, 1000);
		main.tangencialAccelVariance.setRange(0, 1000);
		main.rotationIsDir.setSelection(false);
		
		main.startRadius.setRange(0, 500);
		main.startRadiusVariance.setRange(0, 500);
		main.endRadius.setRange(0, 500);
		main.endRadiusVariance.setRange(0, 500);
		main.rotatePerSecond.setRange(0, 360);
		main.rotatePerSecondVariance.setRange(0, 360);
		
		((GridData)main.gravity.getLayoutData()).exclude = false;
		((GridData)main.radius.getLayoutData()).exclude = true;
		
		main.layout();
		
		main.emitterType.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				handleSwitchType();
			}
		});
		
		main.rotationIsDir.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				makeDirty();
			}
		});
		
		main.texture.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				if (imgTexture != null) {
					GC gc = e.gc;
					Rectangle clientArea = main.texture.getClientArea();
					gc.drawImage(imgTexture, 0, 0, imgTexture.getBounds().width, imgTexture.getBounds().height, 
							clientArea.x, clientArea.y, clientArea.width, clientArea.height);
				}
			}
		});
		
		main.texture.addMouseListener(new MouseAdapter() {
			public void mouseDown(MouseEvent e) {
				IFile psfile = (IFile)getEditorInput().getAdapter(IFile.class);
				ResourceListSelectionDialog dlg = new ResourceListSelectionDialog(getSite().getShell(), psfile.getProject(), IResource.FILE);
				dlg.open();
				Object[] result = dlg.getResult();
				if (result != null && result.length > 0) {
					IFile file = (IFile)dlg.getResult()[0];
					if (!loadImage(file.getLocation())) {
						MessageDialog.openError(getSite().getShell(), "Error", String.format("'%s' is not an image file!", file.getLocation()));
					} else {
						makeDirty();
					}
				}
			}
		});
	}
	
	private boolean loadImage(IPath path) {
		try {
			IFile psfile = (IFile)getEditorInput().getAdapter(IFile.class);
			imgTexture = new Image(null, path.toOSString());
			texturePath = path.makeRelativeTo(psfile.getLocation().removeLastSegments(1)).toString();
			main.texture.redraw();
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
	private void handleSwitchType() {
		((GridData)main.gravity.getLayoutData()).exclude = main.emitterType.getSelectionIndex() != 0;
		((GridData)main.radius.getLayoutData()).exclude = main.emitterType.getSelectionIndex() != 1;
		main.gravity.setVisible(main.emitterType.getSelectionIndex() == 0);
		main.radius.setVisible(main.emitterType.getSelectionIndex() == 1);
		main.radius.getParent().layout();
		makeDirty();
	}

	@Override
	protected void updateData(boolean toModel) {
		if (toModel) {
			model.emissionRate = main.emissionRate.getValue();
			model.maxParticles = (int)main.maxParticles.getValue();
			model.lifeSpan = main.lifeSpan.getValue();
			model.lifeSpanVariance = main.lifeSpanVariance.getValue();
			model.startSize = main.startSize.getValue();
			model.startSizeVariance = main.startSizeVariance.getValue();
			model.finishSize = main.finishSize.getValue();
			model.finishSizeVariance = main.finishSizeVariance.getValue();
			model.emitAngle = main.emitAngle.getValue();
			model.emitAngleVariance = main.emitAngleVariance.getValue();
			model.rotationStart = main.rotationStart.getValue();
			model.rotationStartVariance = main.rotationStartVariance.getValue();
			model.rotationEnd = main.rotationEnd.getValue();
			model.rotationEndVariance = main.rotationEndVariance.getValue();
			
			model.startRed = main.startRed.getValue();
			model.startGreen = main.startGreen.getValue();
			model.startBlue = main.startBlue.getValue();
			model.startAlpha = main.startAlpha.getValue();
			model.startRedVariance = main.startRedVariance.getValue();
			model.startGreenVariance = main.startGreenVariance.getValue();
			model.startBlueVariance = main.startBlueVariance.getValue();
			model.startAlphaVariance = main.startAlphaVariance.getValue();

			model.finishRed = main.finishRed.getValue();
			model.finishGreen = main.finishGreen.getValue();
			model.finishBlue = main.finishBlue.getValue();
			model.finishAlpha = main.finishAlpha.getValue();
			model.finishRedVariance = main.finishRedVariance.getValue();
			model.finishGreenVariance = main.finishGreenVariance.getValue();
			model.finishBlueVariance = main.finishBlueVariance.getValue();
			model.finishAlphaVariance = main.finishAlphaVariance.getValue();
			
			model.duration = main.duration.getValue();
			if (main.emitterType.getSelectionIndex() == 0) model.type = "gravity";
			else if (main.emitterType.getSelectionIndex() == 1) model.type = "radius";
			model.sourcePositionX = main.sourcePositionX.getValue();
			model.sourcePositionXVariance = main.sourcePositionXVariance.getValue();
			model.sourcePositionY = main.sourcePositionY.getValue();
			model.sourcePositionYVariance = main.sourcePositionYVariance.getValue();
			
			model.speed = main.speed.getValue();
			model.speedVariance = main.speedVariance.getValue();
			model.gravityX = main.gravityX.getValue();
			model.gravityY = main.gravityY.getValue();
			model.radialAccel = main.radialAccel.getValue();
			model.radialAccelVariance = main.radialAccelVariance.getValue();
			model.tangencialAccel = main.tangencialAccel.getValue();
			model.tangencialAccelVariance = main.tangencialAccelVariance.getValue();
			model.rotationIsDir = main.rotationIsDir.getSelection();

			model.startRadius = main.startRadius.getValue();
			model.startRadiusVariance = main.startRadiusVariance.getValue();
			model.endRadius = main.endRadius.getValue();
			model.endRadiusVariance = main.endRadiusVariance.getValue();
			model.rotatePerSecond = main.rotatePerSecond.getValue();
			model.rotatePerSecondVariance = main.rotatePerSecondVariance.getValue();
			
			model.texture = imgTexture != null ? texturePath : null;
		} else {
			main.emissionRate.setValue(model.emissionRate);
			main.maxParticles.setValue(model.maxParticles);
			main.lifeSpan.setValue(model.lifeSpan);
			main.lifeSpanVariance.setValue(model.lifeSpanVariance);
			main.startSize.setValue(model.startSize);
			main.startSizeVariance.setValue(model.startSizeVariance);
			main.finishSize.setValue(model.finishSize);
			main.finishSizeVariance.setValue(model.finishSizeVariance);
			main.emitAngle.setValue(model.emitAngle);
			main.emitAngleVariance.setValue(model.emitAngleVariance);
			main.rotationStart.setValue(model.rotationStart);
			main.rotationStartVariance.setValue(model.rotationStartVariance);
			main.rotationEnd.setValue(model.rotationEnd);
			main.rotationEndVariance.setValue(model.rotationEndVariance);
			
			main.startRed.setValue(model.startRed);
			main.startGreen.setValue(model.startGreen);
			main.startBlue.setValue(model.startBlue);
			main.startAlpha.setValue(model.startAlpha);
			main.startRedVariance.setValue(model.startRedVariance);
			main.startGreenVariance.setValue(model.startGreenVariance);
			main.startBlueVariance.setValue(model.startBlueVariance);
			main.startAlphaVariance.setValue(model.startAlphaVariance);

			main.finishRed.setValue(model.finishRed);
			main.finishGreen.setValue(model.finishGreen);
			main.finishBlue.setValue(model.finishBlue);
			main.finishAlpha.setValue(model.finishAlpha);
			main.finishRedVariance.setValue(model.finishRedVariance);
			main.finishGreenVariance.setValue(model.finishGreenVariance);
			main.finishBlueVariance.setValue(model.finishBlueVariance);
			main.finishAlphaVariance.setValue(model.finishAlphaVariance);
			
			main.duration.setValue(model.duration);
			if (model.type == null) model.type = "gravity";
			if (model.type.compareToIgnoreCase("gravity") == 0) main.emitterType.select(0);
			else if (model.type.compareToIgnoreCase("radius") == 0) main.emitterType.select(1);
			main.sourcePositionX.setValue(model.sourcePositionX);
			main.sourcePositionXVariance.setValue(model.sourcePositionXVariance);
			main.sourcePositionY.setValue(model.sourcePositionY);
			main.sourcePositionYVariance.setValue(model.sourcePositionYVariance);
			
			main.speed.setValue(model.speed);
			main.speedVariance.setValue(model.speedVariance);
			main.gravityX.setValue(model.gravityX);
			main.gravityY.setValue(model.gravityY);
			main.radialAccel.setValue(model.radialAccel);
			main.radialAccelVariance.setValue(model.radialAccelVariance);
			main.tangencialAccel.setValue(model.tangencialAccel);
			main.tangencialAccelVariance.setValue(model.tangencialAccelVariance);
			main.rotationIsDir.setSelection(model.rotationIsDir);
			
			main.startRadius.setValue(model.startRadius);
			main.startRadiusVariance.setValue(model.startRadiusVariance);
			main.endRadius.setValue(model.endRadius);
			main.endRadiusVariance.setValue(model.endRadiusVariance);
			main.rotatePerSecond.setValue(model.rotatePerSecond);
			main.rotatePerSecondVariance.setValue(model.rotatePerSecondVariance);
			
			if (model.texture != null) {
				IFile psfile = (IFile)getEditorInput().getAdapter(IFile.class);
				IPath texturePath = psfile.getLocation().removeLastSegments(1).append(model.texture);
				loadImage(texturePath);
			} else {
				imgTexture = null;
			}
			
			handleSwitchType();
		}
	}

}
