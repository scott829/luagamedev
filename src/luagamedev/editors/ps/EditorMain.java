package luagamedev.editors.ps;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;

public class EditorMain extends Composite {
	public ValueScaleHorz maxParticles;
	public ValueScaleHorz finishSize;
	public ValueScaleHorz finishSizeVariance;
	public ValueScaleHorz rotationEnd;
	public ValueScaleHorz rotationStartVariance;
	public ValueScaleHorz lifeSpanVariance;
	public ValueScaleHorz lifeSpan;
	public ValueScaleHorz startSize;
	public ValueScaleHorz startSizeVariance;
	public ValueScaleHorz emitAngleVariance;
	public ValueScaleHorz rotationStart;
	public ValueScaleHorz rotationEndVariance;
	public ValueScaleHorz emitAngle;
	public ValueScaleHorz startAlphaVariance;
	public Canvas texture;
	public ValueScaleHorz finishRed;
	public ValueScaleHorz startBlueVariance;
	public ValueScaleHorz startBlue;
	public ValueScaleHorz startRed;
	public ValueScaleHorz finishGreenVariance;
	public ValueScaleHorz startGreen;
	public ValueScaleHorz startRedVariance;
	public ValueScaleHorz finishAlphaVariance;
	public ValueScaleHorz startAlpha;
	public ValueScaleHorz finishGreen;
	public ValueScaleHorz finishBlueVariance;
	public ValueScaleHorz finishRedVariance;
	public ValueScaleHorz finishAlpha;
	public ValueScaleHorz finishBlue;
	public ValueScaleHorz startGreenVariance;
	public Button rotationIsDir;
	public ValueScaleHorz radialAccel;
	public ValueScaleHorz duration;
	public ValueScaleHorz tangencialAccelVariance;
	public ValueScaleHorz sourcePositionX;
	public ValueScaleHorz tangencialAccel;
	public ValueScaleHorz radialAccelVariance;
	public ValueScaleHorz sourcePositionXVariance;
	public ValueScaleHorz gravityX;
	public ValueScaleHorz sourcePositionYVariance;
	public ValueScaleHorz sourcePositionY;
	public Combo emitterType;
	public ValueScaleHorz gravityY;
	public ValueScaleHorz emissionRate;
	public ValueScaleHorz speedVariance;
	public ValueScaleHorz speed;
	public Group radius;
	public Group gravity;
	public ValueScaleHorz endRadiusVariance;
	public ValueScaleHorz rotatePerSecondVariance;
	public ValueScaleHorz startRadiusVariance;
	public ValueScaleHorz rotatePerSecond;
	public ValueScaleHorz startRadius;
	public ValueScaleHorz endRadius;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public EditorMain(Composite parent, int style) {
		super(parent, SWT.NONE);
		setLayout(new FillLayout(SWT.HORIZONTAL));
		
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		TabFolder tabFolder = new TabFolder(composite, SWT.NONE);
		
		TabItem tbtmNewItem = new TabItem(tabFolder, SWT.NONE);
		tbtmNewItem.setText("Particle Configuration");
		
		Composite group = new Composite(tabFolder, SWT.NONE);
		tbtmNewItem.setControl(group);
		GridLayout gl_group = new GridLayout(2, false);
		gl_group.verticalSpacing = 0;
		group.setLayout(gl_group);
		
		Label label_8 = new Label(group, SWT.NONE);
		label_8.setText("Max Particles");
		
		maxParticles = new ValueScaleHorz(group, SWT.NONE);
		GridData gd_maxParticles = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_maxParticles.widthHint = 157;
		maxParticles.setLayoutData(gd_maxParticles);
		
		Label label_9 = new Label(group, SWT.NONE);
		label_9.setText("Lifespan");
		
		lifeSpan = new ValueScaleHorz(group, SWT.NONE);
		
		Label label_10 = new Label(group, SWT.NONE);
		label_10.setText("Lifespan Variance");
		
		lifeSpanVariance = new ValueScaleHorz(group, SWT.NONE);
		
		Label label_11 = new Label(group, SWT.NONE);
		label_11.setText("Start size");
		
		startSize = new ValueScaleHorz(group, SWT.NONE);
		
		Label label_12 = new Label(group, SWT.NONE);
		label_12.setText("Start Size Variance");
		
		startSizeVariance = new ValueScaleHorz(group, SWT.NONE);
		
		Label label_13 = new Label(group, SWT.NONE);
		label_13.setText("Finish Size");
		
		finishSize = new ValueScaleHorz(group, SWT.NONE);
		
		Label label_14 = new Label(group, SWT.NONE);
		label_14.setText("Finish Size Variance");
		
		finishSizeVariance = new ValueScaleHorz(group, SWT.NONE);
		
		Label label_15 = new Label(group, SWT.NONE);
		label_15.setText("Particle Emit Angle");
		
		emitAngle = new ValueScaleHorz(group, SWT.NONE);
		
		Label label_16 = new Label(group, SWT.NONE);
		label_16.setText("Particle Emit Angle Variance");
		
		emitAngleVariance = new ValueScaleHorz(group, SWT.NONE);
		
		Label label_17 = new Label(group, SWT.NONE);
		label_17.setText("Rotation Start");
		
		rotationStart = new ValueScaleHorz(group, SWT.NONE);
		
		Label label_18 = new Label(group, SWT.NONE);
		label_18.setText("Rotation Start Variance");
		
		rotationStartVariance = new ValueScaleHorz(group, SWT.NONE);
		
		Label label_19 = new Label(group, SWT.NONE);
		label_19.setText("Rotation End");
		
		rotationEnd = new ValueScaleHorz(group, SWT.NONE);
		
		Label label_20 = new Label(group, SWT.NONE);
		label_20.setText("Rotation End Variance");
		
		rotationEndVariance = new ValueScaleHorz(group, SWT.NONE);
		
		TabItem Color = new TabItem(tabFolder, SWT.NONE);
		Color.setText("Particle Color");
		
		Composite composite_3 = new Composite(tabFolder, SWT.NONE);
		Color.setControl(composite_3);
		GridLayout gl_composite_3 = new GridLayout(2, false);
		gl_composite_3.marginHeight = 0;
		gl_composite_3.verticalSpacing = 0;
		composite_3.setLayout(gl_composite_3);
		
		Group grpStart = new Group(composite_3, SWT.NONE);
		grpStart.setText("Start");
		GridLayout gl_grpStart = new GridLayout(2, false);
		gl_grpStart.verticalSpacing = 0;
		gl_grpStart.horizontalSpacing = 0;
		grpStart.setLayout(gl_grpStart);
		
		Label lblRed = new Label(grpStart, SWT.NONE);
		lblRed.setText("Red");
		
		startRed = new ValueScaleHorz(grpStart, SWT.NONE);
		
		Label lblGreen = new Label(grpStart, SWT.NONE);
		lblGreen.setText("Green");
		
		startGreen = new ValueScaleHorz(grpStart, SWT.NONE);
		
		Label lblNewLabel = new Label(grpStart, SWT.NONE);
		lblNewLabel.setText("Blue");
		
		startBlue = new ValueScaleHorz(grpStart, SWT.NONE);
		
		Label lblNewLabel_1 = new Label(grpStart, SWT.NONE);
		lblNewLabel_1.setText("Alpha");
		
		startAlpha = new ValueScaleHorz(grpStart, SWT.NONE);
		
		Group grpEnd = new Group(composite_3, SWT.NONE);
		grpEnd.setText("End");
		GridLayout gl_grpEnd = new GridLayout(2, false);
		gl_grpEnd.verticalSpacing = 0;
		gl_grpEnd.horizontalSpacing = 0;
		grpEnd.setLayout(gl_grpEnd);
		
		Label label_5 = new Label(grpEnd, SWT.NONE);
		label_5.setText("Red");
		
		finishRed = new ValueScaleHorz(grpEnd, SWT.NONE);
		
		Label label_6 = new Label(grpEnd, SWT.NONE);
		label_6.setText("Green");
		
		finishGreen = new ValueScaleHorz(grpEnd, SWT.NONE);
		
		Label label_7 = new Label(grpEnd, SWT.NONE);
		label_7.setText("Blue");
		
		finishBlue = new ValueScaleHorz(grpEnd, SWT.NONE);
		
		Label label_21 = new Label(grpEnd, SWT.NONE);
		label_21.setText("Alpha");
		
		finishAlpha = new ValueScaleHorz(grpEnd, SWT.NONE);
		
		Group grpStartVariance = new Group(composite_3, SWT.NONE);
		grpStartVariance.setText("Start Variance");
		GridLayout gl_grpStartVariance = new GridLayout(2, false);
		gl_grpStartVariance.verticalSpacing = 0;
		gl_grpStartVariance.horizontalSpacing = 0;
		grpStartVariance.setLayout(gl_grpStartVariance);
		
		Label label_28 = new Label(grpStartVariance, SWT.NONE);
		label_28.setText("Red");
		
		startRedVariance = new ValueScaleHorz(grpStartVariance, SWT.NONE);
		
		Label label_29 = new Label(grpStartVariance, SWT.NONE);
		label_29.setText("Green");
		
		startGreenVariance = new ValueScaleHorz(grpStartVariance, SWT.NONE);
		
		Label label_30 = new Label(grpStartVariance, SWT.NONE);
		label_30.setText("Blue");
		
		startBlueVariance = new ValueScaleHorz(grpStartVariance, SWT.NONE);
		
		Label label_31 = new Label(grpStartVariance, SWT.NONE);
		label_31.setText("Alpha");
		
		startAlphaVariance = new ValueScaleHorz(grpStartVariance, SWT.NONE);
		
		Group grpEndVariance = new Group(composite_3, SWT.NONE);
		grpEndVariance.setText("End Variance");
		GridLayout gl_grpEndVariance = new GridLayout(2, false);
		gl_grpEndVariance.verticalSpacing = 0;
		gl_grpEndVariance.horizontalSpacing = 0;
		grpEndVariance.setLayout(gl_grpEndVariance);
		
		Label label_32 = new Label(grpEndVariance, SWT.NONE);
		label_32.setText("Red");
		
		finishRedVariance = new ValueScaleHorz(grpEndVariance, SWT.NONE);
		
		Label label_33 = new Label(grpEndVariance, SWT.NONE);
		label_33.setText("Green");
		
		finishGreenVariance = new ValueScaleHorz(grpEndVariance, SWT.NONE);
		
		Label label_34 = new Label(grpEndVariance, SWT.NONE);
		label_34.setText("Blue");
		
		finishBlueVariance = new ValueScaleHorz(grpEndVariance, SWT.NONE);
		
		Label label_35 = new Label(grpEndVariance, SWT.NONE);
		label_35.setText("Alpha");
		
		finishAlphaVariance = new ValueScaleHorz(grpEndVariance, SWT.NONE);
		
		Label lblTexture = new Label(composite_3, SWT.NONE);
		lblTexture.setText("Texture");
		new Label(composite_3, SWT.NONE);
		
		texture = new Canvas(composite_3, SWT.BORDER);
		GridData gd_texture = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_texture.heightHint = 200;
		gd_texture.widthHint = 200;
		texture.setLayoutData(gd_texture);
		new Label(composite_3, SWT.NONE);
		
		TabItem tbtmEmit = new TabItem(tabFolder, SWT.NONE);
		tbtmEmit.setText("Emit");
		
		Composite composite_1 = new Composite(tabFolder, SWT.NONE);
		tbtmEmit.setControl(composite_1);
		composite_1.setLayout(new GridLayout(1, false));
		
		Composite group_1 = new Composite(composite_1, SWT.NONE);
		group_1.setLayout(new GridLayout(2, false));
		
		Composite composite_4 = new Composite(group_1, SWT.NONE);
		composite_4.setLayout(new GridLayout(1, false));
		
		Label lblType = new Label(composite_4, SWT.NONE);
		lblType.setText("Type");
		
		emitterType = new Combo(composite_4, SWT.READ_ONLY);
		emitterType.setItems(new String[] {"Gravity", "Radius"});
		emitterType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label label_4 = new Label(composite_4, SWT.NONE);
		label_4.setText("Duration");
		
		duration = new ValueScaleHorz(composite_4, SWT.NONE);
		
		Label lblNewLabel_2 = new Label(composite_4, SWT.NONE);
		lblNewLabel_2.setText("Rate");
		
		emissionRate = new ValueScaleHorz(composite_4, SWT.NONE);
		
		Group composite_2 = new Group(group_1, SWT.NONE);
		composite_2.setText("Location");
		GridLayout gl_composite_2 = new GridLayout(2, false);
		gl_composite_2.verticalSpacing = 0;
		gl_composite_2.horizontalSpacing = 0;
		composite_2.setLayout(gl_composite_2);
		
		Label label = new Label(composite_2, SWT.NONE);
		label.setText("Source Pos X");
		
		sourcePositionX = new ValueScaleHorz(composite_2, SWT.NONE);
		
		Label label_1 = new Label(composite_2, SWT.NONE);
		label_1.setText("Source Pos X Variance");
		
		sourcePositionXVariance = new ValueScaleHorz(composite_2, SWT.NONE);
		
		Label label_2 = new Label(composite_2, SWT.NONE);
		label_2.setText("Source Pos Y");
		
		sourcePositionY = new ValueScaleHorz(composite_2, SWT.NONE);
		
		Label label_3 = new Label(composite_2, SWT.NONE);
		label_3.setText("Source Pos Y Variance");
		
		sourcePositionYVariance = new ValueScaleHorz(composite_2, SWT.NONE);
		
		gravity = new Group(composite_1, SWT.NONE);
		gravity.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		gravity.setText("Gravity Configuration");
		GridLayout gl_gravity = new GridLayout(2, false);
		gl_gravity.verticalSpacing = 0;
		gl_gravity.horizontalSpacing = 0;
		gravity.setLayout(gl_gravity);
		
		Label lblSpeed = new Label(gravity, SWT.NONE);
		lblSpeed.setText("Speed");
		
		speed = new ValueScaleHorz(gravity, SWT.NONE);
		
		Label lblSpeedVariance = new Label(gravity, SWT.NONE);
		lblSpeedVariance.setText("Speed Variance");
		
		speedVariance = new ValueScaleHorz(gravity, SWT.NONE);
		
		Label lblGravityX = new Label(gravity, SWT.NONE);
		lblGravityX.setText("Gravity X");
		
		gravityX = new ValueScaleHorz(gravity, SWT.NONE);
		
		Label lblGravityY = new Label(gravity, SWT.NONE);
		lblGravityY.setText("Gravity Y");
		
		gravityY = new ValueScaleHorz(gravity, SWT.NONE);
		
		Label lblRadialAccel = new Label(gravity, SWT.NONE);
		lblRadialAccel.setText("Radial Accel");
		
		radialAccel = new ValueScaleHorz(gravity, SWT.NONE);
		
		Label lblRadialAccelVariance = new Label(gravity, SWT.NONE);
		lblRadialAccelVariance.setText("Radial Accel Variance");
		
		radialAccelVariance = new ValueScaleHorz(gravity, SWT.NONE);
		
		Label lblTangencialAccel = new Label(gravity, SWT.NONE);
		lblTangencialAccel.setText("Tangencial Accel");
		
		tangencialAccel = new ValueScaleHorz(gravity, SWT.NONE);
		
		Label lblTangencialAccelVariance = new Label(gravity, SWT.NONE);
		lblTangencialAccelVariance.setText("Tangencial Accel Variance");
		
		tangencialAccelVariance = new ValueScaleHorz(gravity, SWT.NONE);
		
		rotationIsDir = new Button(gravity, SWT.CHECK);
		rotationIsDir.setText("Rotation Is Direction");
		new Label(gravity, SWT.NONE);
		
		radius = new Group(composite_1, SWT.NONE);
		radius.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		radius.setText("Radius Configuration");
		GridLayout gl_radius = new GridLayout(2, false);
		gl_radius.verticalSpacing = 0;
		gl_radius.horizontalSpacing = 0;
		radius.setLayout(gl_radius);
		
		Label label_22 = new Label(radius, SWT.NONE);
		label_22.setText("Start Radius");
		
		startRadius = new ValueScaleHorz(radius, SWT.NONE);
		
		Label label_23 = new Label(radius, SWT.NONE);
		label_23.setText("Start Radius Variance");
		
		startRadiusVariance = new ValueScaleHorz(radius, SWT.NONE);
		
		Label label_24 = new Label(radius, SWT.NONE);
		label_24.setText("End Radius");
		
		endRadius = new ValueScaleHorz(radius, SWT.NONE);
		
		Label label_25 = new Label(radius, SWT.NONE);
		label_25.setText("End Radius Variance");
		
		endRadiusVariance = new ValueScaleHorz(radius, SWT.NONE);
		
		Label label_26 = new Label(radius, SWT.NONE);
		label_26.setText("Rotate Per Second");
		
		rotatePerSecond = new ValueScaleHorz(radius, SWT.NONE);
		
		Label label_27 = new Label(radius, SWT.NONE);
		label_27.setText("Rotate Per Second Variance");
		
		rotatePerSecondVariance = new ValueScaleHorz(radius, SWT.NONE);

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	public ValueScaleHorz getMaxParticles() {
		return maxParticles;
	}
	public ValueScaleHorz getFinishSize() {
		return finishSize;
	}
	public ValueScaleHorz getFinishSizeVariance() {
		return finishSizeVariance;
	}
	public ValueScaleHorz getRotationEnd() {
		return rotationEnd;
	}
	public ValueScaleHorz getRotationStartVariance() {
		return rotationStartVariance;
	}
	public ValueScaleHorz getLifeSpanVariance() {
		return lifeSpanVariance;
	}
	public ValueScaleHorz getLifeSpan() {
		return lifeSpan;
	}
	public ValueScaleHorz getStartSize() {
		return startSize;
	}
	public ValueScaleHorz getStartSizeVariance() {
		return startSizeVariance;
	}
	public ValueScaleHorz getEmitAngleVariance() {
		return emitAngleVariance;
	}
	public ValueScaleHorz getRotationStart() {
		return rotationStart;
	}
	public ValueScaleHorz getRotationEndVariance() {
		return rotationEndVariance;
	}
	public ValueScaleHorz getEmitAngle() {
		return emitAngle;
	}
	public ValueScaleHorz getStartAlphaVariance() {
		return startAlphaVariance;
	}
	public Canvas getTexture() {
		return texture;
	}
	public ValueScaleHorz getFinishRed() {
		return finishRed;
	}
	public ValueScaleHorz getStartBlueVariance() {
		return startBlueVariance;
	}
	public ValueScaleHorz getStartBlue() {
		return startBlue;
	}
	public ValueScaleHorz getStartRed() {
		return startRed;
	}
	public ValueScaleHorz getFinishGreenVariance() {
		return finishGreenVariance;
	}
	public ValueScaleHorz getStartGreen() {
		return startGreen;
	}
	public ValueScaleHorz getStartRedVariance() {
		return startRedVariance;
	}
	public ValueScaleHorz getFinishAlphaVariance() {
		return finishAlphaVariance;
	}
	public ValueScaleHorz getStartAlpha() {
		return startAlpha;
	}
	public ValueScaleHorz getFinishGreen() {
		return finishGreen;
	}
	public ValueScaleHorz getFinishBlueVariance() {
		return finishBlueVariance;
	}
	public ValueScaleHorz getFinishRedVariance() {
		return finishRedVariance;
	}
	public ValueScaleHorz getFinishAlpha() {
		return finishAlpha;
	}
	public ValueScaleHorz getFinishBlue() {
		return finishBlue;
	}
	public ValueScaleHorz getStartGreenVariance() {
		return startGreenVariance;
	}
	public Button getRotationIsDir() {
		return rotationIsDir;
	}
	public ValueScaleHorz getRadialAccel() {
		return radialAccel;
	}
	public ValueScaleHorz getDuration() {
		return duration;
	}
	public ValueScaleHorz getTangencialAccelVariance() {
		return tangencialAccelVariance;
	}
	public ValueScaleHorz getSourcePositionX() {
		return sourcePositionX;
	}
	public ValueScaleHorz getTangencialAccel() {
		return tangencialAccel;
	}
	public ValueScaleHorz getRadialAccelVariance() {
		return radialAccelVariance;
	}
	public ValueScaleHorz getSourcePositionXVariance() {
		return sourcePositionXVariance;
	}
	public ValueScaleHorz getGravityX() {
		return gravityX;
	}
	public ValueScaleHorz getSourcePositionYVariance() {
		return sourcePositionYVariance;
	}
	public ValueScaleHorz getSourcePositionY() {
		return sourcePositionY;
	}
	public Combo getEmitterType() {
		return emitterType;
	}
	public ValueScaleHorz getGravityY() {
		return gravityY;
	}
	public ValueScaleHorz getEmissionRate() {
		return emissionRate;
	}
	public ValueScaleHorz getSpeedVariance() {
		return speedVariance;
	}
	public ValueScaleHorz getSpeed() {
		return speed;
	}
	public Group getRadius() {
		return radius;
	}
	public Group getGravity() {
		return gravity;
	}
	public ValueScaleHorz getEndRadiusVariance() {
		return endRadiusVariance;
	}
	public ValueScaleHorz getValueScaleHorz_27() {
		return rotatePerSecondVariance;
	}
	public ValueScaleHorz getStartRadiusVariance() {
		return startRadiusVariance;
	}
	public ValueScaleHorz getRotatePerSecond() {
		return rotatePerSecond;
	}
	public ValueScaleHorz getStartRadius() {
		return startRadius;
	}
	public ValueScaleHorz getEndRadius() {
		return endRadius;
	}
}
