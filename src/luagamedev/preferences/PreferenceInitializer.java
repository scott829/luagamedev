package luagamedev.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import luagamedev.LuaGamePlugin;;

public class PreferenceInitializer extends AbstractPreferenceInitializer {

	@Override
	public void initializeDefaultPreferences() {
		// TODO 自动生成的方法存根
		IPreferenceStore store = LuaGamePlugin.getDefault().getPreferenceStore();
		store.setDefault(PreferenceConstants.P_LAUNCHER_PATH, "c:\\luagame.exe");
		store.setDefault(PreferenceConstants.P_DEBUG_PORT, 39598);
	}

}
