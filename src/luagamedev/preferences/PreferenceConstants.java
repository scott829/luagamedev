package luagamedev.preferences;

public class PreferenceConstants {
	public static final String P_LAUNCHER_PATH = "launcherPath";
	
	public static final String P_DEBUG_PORT = "debugPort";
}
