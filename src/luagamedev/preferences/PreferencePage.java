package luagamedev.preferences;

import luagamedev.LuaGamePlugin;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class PreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	
	public PreferencePage() {
		super(FieldEditorPreferencePage.GRID);
		setPreferenceStore(LuaGamePlugin.getDefault().getPreferenceStore());
	}

	@Override
	protected void createFieldEditors() {
		// TODO 自动生成的方法存根
		addField(new FileFieldEditor(
				PreferenceConstants.P_LAUNCHER_PATH,
				"Launcher &Location:",
				true,
				getFieldEditorParent()
				));
		
		addField(new IntegerFieldEditor(PreferenceConstants.P_DEBUG_PORT, 
				"Debug &Port:", 
				getFieldEditorParent()
				));
	}

	@Override
	public void init(IWorkbench workbench) {
		// TODO 自动生成的方法存根
		
	}

}
