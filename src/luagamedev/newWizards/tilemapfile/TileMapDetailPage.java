package luagamedev.newWizards.tilemapfile;

import java.io.InputStreamReader;

import luagamedev.editors.model.Properties;
import luagamedev.editors.tile.model.TileModel;
import luagamedev.util.NumberVerify;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Button;
import org.eclipse.ui.dialogs.ResourceListSelectionDialog;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class TileMapDetailPage extends WizardPage {
	
	private IContainer newContainer;
	public Text mapWidthText;
	public Text mapHeightText;
	public Text tileWidthText;
	public Text tileHeightText;
	public List tilesetList;
	public Combo typeCombo;

	/**
	 * Create the wizard.
	 */
	public TileMapDetailPage(IContainer container) {
		super("wizardPage");
		this.newContainer = container;
		setTitle("Tile map detail config");
		setDescription("Config tile map detail options");
		setPageComplete(false);
	}

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);

		setControl(container);
		container.setLayout(new GridLayout(1, false));
		
		Composite composite = new Composite(container, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));
		
		Label lblNewLabel = new Label(composite, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel.setText("Type:");
		
		typeCombo = new Combo(composite, SWT.READ_ONLY);
		typeCombo.setItems(new String[] {"Ortho", "Iso"});
		typeCombo.select(0);
		GridData gd_typeCombo = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_typeCombo.widthHint = 100;
		typeCombo.setLayoutData(gd_typeCombo);
		
		Group grpMapSize = new Group(container, SWT.NONE);
		grpMapSize.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpMapSize.setText("Map Size");
		grpMapSize.setLayout(new GridLayout(4, false));
		
		Label label = new Label(grpMapSize, SWT.NONE);
		label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		label.setText("Width:");
		
		mapWidthText = new Text(grpMapSize, SWT.BORDER);
		mapWidthText.setText("50");
		GridData gd_mapWidthText = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_mapWidthText.widthHint = 50;
		mapWidthText.setLayoutData(gd_mapWidthText);
		mapWidthText.addListener(SWT.Verify, new NumberVerify(false));
		
		Label label_1 = new Label(grpMapSize, SWT.NONE);
		label_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		label_1.setText("Height:");
		
		mapHeightText = new Text(grpMapSize, SWT.BORDER);
		mapHeightText.setText("50");
		GridData gd_mapHeightText = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_mapHeightText.widthHint = 50;
		mapHeightText.setLayoutData(gd_mapHeightText);
		mapHeightText.addListener(SWT.Verify, new NumberVerify(false));
		
		Group group_1 = new Group(container, SWT.NONE);
		group_1.setText("Tile Size");
		group_1.setLayout(new GridLayout(4, false));
		
		Label label_2 = new Label(group_1, SWT.NONE);
		label_2.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		label_2.setText("Width:");
		
		tileWidthText = new Text(group_1, SWT.BORDER);
		tileWidthText.setText("32");
		GridData gd_tileWidthText = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_tileWidthText.widthHint = 50;
		tileWidthText.setLayoutData(gd_tileWidthText);
		tileWidthText.addListener(SWT.Verify, new NumberVerify(false));
		
		Label label_3 = new Label(group_1, SWT.NONE);
		label_3.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		label_3.setText("Height:");
		
		tileHeightText = new Text(group_1, SWT.BORDER);
		tileHeightText.setText("32");
		GridData gd_tileHeightText = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_tileHeightText.widthHint = 50;
		tileHeightText.setLayoutData(gd_tileHeightText);
		tileHeightText.addListener(SWT.Verify, new NumberVerify(false));
		
		Group grpTilesets = new Group(container, SWT.NONE);
		grpTilesets.setLayout(new GridLayout(2, false));
		grpTilesets.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpTilesets.setText("Tilesets");
		
		tilesetList = new List(grpTilesets, SWT.BORDER);
		tilesetList.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		Composite composite_1 = new Composite(grpTilesets, SWT.NONE);
		GridData gd_composite_1 = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
		gd_composite_1.widthHint = 80;
		composite_1.setLayoutData(gd_composite_1);
		composite_1.setLayout(new GridLayout(1, false));
		
		Button btnAddTileset = new Button(composite_1, SWT.NONE);
		btnAddTileset.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnAddTileset.setBounds(0, 0, 80, 27);
		btnAddTileset.setText("&Add...");
		btnAddTileset.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ResourceListSelectionDialog dlg = new ResourceListSelectionDialog(getShell(), newContainer.getProject(), IResource.FILE);
				dlg.setTitle("Add TileSet file");
				dlg.open();
				Object[] result = dlg.getResult();
				if (result != null && result.length > 0) {
					IFile file = (IFile)dlg.getResult()[0];
					if (checkTileSet(file)) {
						tilesetList.add(file.getLocation().makeRelativeTo(newContainer.getLocation()).toString());
						updatePage();
					} else {
						MessageDialog.openError(getShell(), "Error", String.format("'%s' is not a TileSet file!", file.getLocation()));
					}
				}

			}
		});
		
		Button btnDelTileset = new Button(composite_1, SWT.NONE);
		btnDelTileset.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDelTileset.setBounds(0, 0, 80, 27);
		btnDelTileset.setText("&Delete");
		btnDelTileset.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (tilesetList.getSelectionIndex() != -1) {
					tilesetList.remove(tilesetList.getSelectionIndex());
					updatePage();
				}
			}
		});
	}
	
	private boolean checkTileSet(IFile tileFile) {
		try {
			GsonBuilder builder = new GsonBuilder();
			builder.registerTypeAdapter(Properties.class, new Properties.Adapter());
			Gson gson = builder.create();
			TileModel model = gson.fromJson(new InputStreamReader(tileFile.getContents()), TileModel.class);
			if (model != null) {
				// 得到image文件的绝对路径
				IPath imgAbsPath = tileFile.getLocation().removeLastSegments(1).append(model.filename);
				Image img = new Image(Display.getCurrent(), imgAbsPath.toString());
				if (img != null) {
					img.dispose();
					return true;
				}
			}
		} catch (Exception e) {
		}
		
		return false;
	}
	
	private void updatePage() {
		setPageComplete(
				mapWidthText.getText().length() > 0 && Integer.valueOf(mapWidthText.getText()) > 0 &&
				mapHeightText.getText().length() > 0 && Integer.valueOf(mapHeightText.getText()) > 0 &&
				tileWidthText.getText().length() > 0 && Integer.valueOf(tileWidthText.getText()) > 0 &&
				tileHeightText.getText().length() > 0 && Integer.valueOf(tileHeightText.getText()) > 0 &&
				tilesetList.getItemCount() > 0);
	}
}
