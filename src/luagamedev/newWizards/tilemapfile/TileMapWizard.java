package luagamedev.newWizards.tilemapfile;

import java.io.ByteArrayInputStream;

import luagamedev.editors.model.Properties;
import luagamedev.editors.tilemap.model.TileMapModel;
import luagamedev.editors.tilemap.model.TileMapModel.Layer;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.wizards.newresource.BasicNewFileResourceWizard;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class TileMapWizard extends BasicNewFileResourceWizard implements INewWizard {
	private TileMapDetailPage detailPage;
	
	public TileMapWizard() {
		super();
		setWindowTitle("Create Tile Map File");
		setNeedsProgressMonitor(true);
	}
	
	public void addPages() {
		super.addPages();
		
		IContainer container = null;
		IStructuredSelection structuredSelection = getSelection();
		
		if (structuredSelection.size() == 1) {
			Object object = structuredSelection.getFirstElement();
			if (object instanceof IAdaptable) {
				container = (IContainer)((IAdaptable)object).getAdapter(IContainer.class);
				if (container == null) {
					IResource res = (IResource)((IAdaptable)object).getAdapter(IResource.class);
					container = res.getParent();
				}
			}
		}

		detailPage = new TileMapDetailPage(container);
		addPage(detailPage);
	}
	
	protected void selectAndReveal(IResource resource) {
		TileMapModel model = new TileMapModel();
		model.type = detailPage.typeCombo.getItem(detailPage.typeCombo.getSelectionIndex());

		model.size = new TileMapModel.Size();
		model.size.width = Integer.valueOf(detailPage.mapWidthText.getText());
		model.size.height = Integer.valueOf(detailPage.mapHeightText.getText());

		model.tilesize = new TileMapModel.Size();
		model.tilesize.width = Integer.valueOf(detailPage.tileWidthText.getText());
		model.tilesize.height = Integer.valueOf(detailPage.tileHeightText.getText());
		
		model.tilesets = detailPage.tilesetList.getItems();
		model.layers = new TileMapModel.Layer[1];
		model.layers[0] = new TileMapModel.Layer();
		model.layers[0].name = "Layer1";
		model.layers[0].data = new int[model.size.width * model.size.height];

		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Properties.class, new Properties.Adapter());
		Gson gson = builder.create();
		String json = gson.toJson(model);
		try {
			((IFile) resource).setContents(
					new ByteArrayInputStream(json.getBytes()), true, false,
					null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
}
