package luagamedev.newWizards.emptyproject;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;

public class ProjectWizard extends BasicNewProjectResourceWizard implements INewWizard {
	public ProjectWizard() {
		super();
		setWindowTitle("Create LuaGame Project");
		setNeedsProgressMonitor(true);
	}

	public boolean performFinish() {
		super.performFinish();
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					doFinish(getNewProject(), monitor);
				} catch (CoreException e) {
					throw new InvocationTargetException(e);
				}
				monitor.done();
			}
		};

		try {
			getContainer().run(true, false, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error", realException.getMessage());
			return false;
		}
		
		return true;
	}
	
	private void setProjectNature(IProject project) {
		try {
			IProjectDescription description;
			description = project.getDescription();
			String[] natures = description.getNatureIds();
			String[] newNatures = new String[natures.length + 1];
			System.arraycopy(natures, 0, newNatures, 0, natures.length);
			newNatures[natures.length] = "LuaGameDev.luagameNature";
			description.setNatureIds(newNatures);
			project.setDescription(description, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	private void doFinish(IProject project, IProgressMonitor monitor) throws CoreException {
		// 创建工程文件夹
		monitor.beginTask("create  " + project.getName(), 5);
		project.open(null);
		setProjectNature(project);
		monitor.worked(1);
		
		IFolder resourceFolder = project.getFolder("resource");
		resourceFolder.create(true,  true, null);
		
		monitor.setTaskName("create global.conf");
		resourceFolder.getFile("global.conf").create(
				this.getClass().getResourceAsStream("/templates/global.conf"), true, monitor);
		monitor.worked(1);
		
		monitor.setTaskName("create media");
		resourceFolder.getFolder("media").create(true, true, null);
		monitor.worked(1);

		monitor.setTaskName("create scripts");
		resourceFolder.getFolder("scripts").create(true, true, null);
		resourceFolder.getFolder("scripts").getFile("main.lua").create(
				this.getClass().getResourceAsStream("/templates/main.lua"), true, monitor);
		monitor.worked(1);

		monitor.setTaskName("create fonts");
		resourceFolder.getFolder("fonts").create(true, true, null);
		monitor.worked(1);
	}
}