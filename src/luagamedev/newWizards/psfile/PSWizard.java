package luagamedev.newWizards.psfile;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.wizards.newresource.BasicNewFileResourceWizard;

public class PSWizard extends BasicNewFileResourceWizard implements INewWizard {
	protected void selectAndReveal(IResource resource){
		try {
			((IFile) resource).setContents(getClass().getResourceAsStream("/templates/template.ps"), true, false,
					null);
		} catch (CoreException e) {
		}
	}
}
