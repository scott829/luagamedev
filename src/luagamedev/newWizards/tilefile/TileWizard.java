package luagamedev.newWizards.tilefile;

import java.io.ByteArrayInputStream;

import luagamedev.editors.model.Properties;
import luagamedev.editors.tile.model.TileModel;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.wizards.newresource.BasicNewFileResourceWizard;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class TileWizard extends BasicNewFileResourceWizard implements INewWizard {
	
	private TileDetailPage detailPage;
	
	public TileWizard() {
		super();
		setWindowTitle("Create Tile File");
		setNeedsProgressMonitor(true);
	}

	public void addPages() {
		super.addPages();
		
		IContainer container = null;
		IStructuredSelection structuredSelection = getSelection();
		
		if (structuredSelection.size() == 1) {
			Object object = structuredSelection.getFirstElement();
			if (object instanceof IAdaptable) {
				container = (IContainer)((IAdaptable)object).getAdapter(IContainer.class);
				if (container == null) {
					IResource res = (IResource)((IAdaptable)object).getAdapter(IResource.class);
					container = res.getParent();
				}
			}
		}

		detailPage = new TileDetailPage(container);
		addPage(detailPage);
	}
		
	protected void selectAndReveal(IResource resource) {
		TileModel model = new TileModel();
		model.name = detailPage.nameText.getText();
		model.filename = detailPage.imageText.getText();
		model.margin = Integer.valueOf(detailPage.marginText.getText());
		model.space = Integer.valueOf(detailPage.spacingText.getText());
		model.size = new TileModel.TileSize();
		model.size.width = Integer.valueOf(detailPage.tileWidthText.getText());
		model.size.height = Integer
				.valueOf(detailPage.tileHeightText.getText());

		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Properties.class, new Properties.Adapter());
		Gson gson = builder.create();
		String json = gson.toJson(model);
		try {
			((IFile) resource).setContents(
					new ByteArrayInputStream(json.getBytes()), true, false,
					null);
		} catch (CoreException e) {
			e.printStackTrace();
		}

	}
}
