package luagamedev.newWizards.tilefile;

import luagamedev.util.NumberVerify;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.ui.dialogs.ResourceListSelectionDialog;

public class TileDetailPage extends WizardPage {
	public Text tileWidthText;
	public Text tileHeightText;
	public Text marginText;
	public Text spacingText;
	public Text imageText;
	public Text nameText;
	private IContainer newContainer;

	/**
	 * Create the wizard.
	 */
	public TileDetailPage(IContainer container) {
		super("wizardPage");
		this.newContainer = container;
		setTitle("Tile detail config");
		setDescription("Config tile detail options");
		setPageComplete(false);
	}

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);

		setControl(container);
		GridLayout gl_container = new GridLayout(1, false);
		container.setLayout(gl_container);
		
		Composite composite = new Composite(container, SWT.NONE);
		composite.setLayout(new GridLayout(3, false));
		
		Label lblNewLabel_5 = new Label(composite, SWT.NONE);
		lblNewLabel_5.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel_5.setText("Name:");
		
		nameText = new Text(composite, SWT.BORDER);
		nameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		nameText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				updatePage();
			}
		});
		new Label(composite, SWT.NONE);
		
		Label lblNewLabel_4 = new Label(composite, SWT.NONE);
		lblNewLabel_4.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel_4.setText("Image Filename:");
		
		imageText = new Text(composite, SWT.BORDER);
		GridData gd_imageText = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_imageText.widthHint = 200;
		imageText.setLayoutData(gd_imageText);
		
		Button btnSelectImage = new Button(composite, SWT.NONE);
		btnSelectImage.setText("&Browser...");
		btnSelectImage.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				ResourceListSelectionDialog dlg = new ResourceListSelectionDialog(getShell(), newContainer.getProject(), 
						IResource.FILE);
				dlg.setTitle("Select image file for tile");
				dlg.open();
				Object[] result = dlg.getResult();
				if (result != null && result.length > 0) {
					IFile file = (IFile)dlg.getResult()[0];
					try {
						Image img = new Image(null, file.getLocation().toOSString());
						imageText.setText(file.getLocation().makeRelativeTo(newContainer.getLocation()).toString());
						updatePage();
						img.dispose();
					} catch(SWTException err) {
						MessageDialog.openError(getShell(), "Error", String.format("'%s' is not image!", file.getLocation()));
					}
				}
			}
		});
		
		Group grpTileSize = new Group(container, SWT.NONE);
		grpTileSize.setText("Tile Size");
		grpTileSize.setLayout(new GridLayout(4, false));
		grpTileSize.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		Label lblNewLabel = new Label(grpTileSize, SWT.NONE);
		lblNewLabel.setText("Width:");
		
		tileWidthText = new Text(grpTileSize, SWT.BORDER);
		tileWidthText.setText("32");
		GridData gd_tileWidthText = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_tileWidthText.widthHint = 50;
		tileWidthText.setLayoutData(gd_tileWidthText);
		tileWidthText.addListener(SWT.Verify, new NumberVerify(false));
		
		Label lblNewLabel_1 = new Label(grpTileSize, SWT.NONE);
		lblNewLabel_1.setText("Height:");
		
		tileHeightText = new Text(grpTileSize, SWT.BORDER);
		tileHeightText.setText("32");
		GridData gd_tileHeightText = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_tileHeightText.widthHint = 50;
		tileHeightText.setLayoutData(gd_tileHeightText);
		tileHeightText.addListener(SWT.Verify, new NumberVerify(false));
		
		Group grpMargin = new Group(container, SWT.NONE);
		grpMargin.setText("Margin and spacing");
		grpMargin.setLayout(new GridLayout(4, false));
		grpMargin.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblNewLabel_2 = new Label(grpMargin, SWT.NONE);
		lblNewLabel_2.setText("Margin:");
		
		marginText = new Text(grpMargin, SWT.BORDER);
		marginText.setText("0");
		GridData gd_marginText = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_marginText.widthHint = 50;
		marginText.setLayoutData(gd_marginText);
		marginText.addListener(SWT.Verify, new NumberVerify(false));
		
		Label lblNewLabel_3 = new Label(grpMargin, SWT.NONE);
		lblNewLabel_3.setText("Spacing:");
		
		spacingText = new Text(grpMargin, SWT.BORDER);
		spacingText.setText("0");
		GridData gd_spacingText = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_spacingText.widthHint = 50;
		spacingText.setLayoutData(gd_spacingText);
		spacingText.addListener(SWT.Verify, new NumberVerify(false));
	}
	
	private void updatePage() {
		setPageComplete(
				nameText.getText().length() > 0 &&
				imageText.getText().length() > 0 &&
				tileWidthText.getText().length() > 0 && Integer.valueOf(tileWidthText.getText()) > 0 &&
				tileHeightText.getText().length() > 0 && Integer.valueOf(tileHeightText.getText()) > 0 &&
				marginText.getText().length() > 0 &&
				spacingText.getText().length() > 0);
	}
}
