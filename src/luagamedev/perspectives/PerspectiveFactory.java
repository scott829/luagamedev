package luagamedev.perspectives;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class PerspectiveFactory implements IPerspectiveFactory {

	@Override
	public void createInitialLayout(IPageLayout layout) {
		IFolderLayout left = layout.createFolder("left", IPageLayout.LEFT, 0.2f, IPageLayout.ID_EDITOR_AREA);
		left.addView(IPageLayout.ID_RES_NAV);
		layout.addActionSet("org.eclipse.debug.ui.launchActionSet");
		
		IFolderLayout bottom = layout.createFolder("bottom", IPageLayout.BOTTOM, 0.3f, IPageLayout.ID_EDITOR_AREA);
	}

}
