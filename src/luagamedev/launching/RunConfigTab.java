package luagamedev.launching;

import luagamedev.LuaGamePlugin;
import luagamedev.preferences.PreferenceConstants;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.dialogs.ResourceListSelectionDialog;

public class RunConfigTab extends AbstractLaunchConfigurationTab {
	
	private Text launcherPath;
	
	private Text projectName;

	@Override
	public void createControl(Composite parent) {
		// TODO 自动生成的方法存根
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		Label launcherPathLabel, projectLabel;
		Button browseLauncherBtn;
		
		container.setLayout(layout);
		layout.numColumns = 3;
		layout.verticalSpacing = 9;

		// 启动器设置
		launcherPathLabel = new Label(container, SWT.NULL);
		launcherPathLabel.setText("Launcher &Location:");

		launcherPath = new Text(container, SWT.BORDER | SWT.SINGLE);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		launcherPath.setLayoutData(gd);
		launcherPath.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				launcherPathChanged();
			}
		});
		
		browseLauncherBtn = new Button(container, SWT.PUSH);
		browseLauncherBtn.setText("&Browser...");
		browseLauncherBtn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleBrowseLauncher();
			}
		});
		
		// 项目设置
		projectLabel = new Label(container, SWT.NULL);
		projectLabel.setText("&Project:");

		projectName = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		projectName.setLayoutData(gd);
		projectName.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				launcherPathChanged();
			}
		});
		
		browseLauncherBtn = new Button(container, SWT.PUSH);
		browseLauncherBtn.setText("&Browser...");
		browseLauncherBtn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleBrowseProject();
			}
		});

		launcherPath.setFocus();
		setControl(container);
	}
	
	private void launcherPathChanged() {
		updateLaunchConfigurationDialog();
	}
	
	private void handleBrowseLauncher() {
		FileDialog dlg = new FileDialog(getShell(), SWT.OPEN);
		String filename = dlg.open();
		if (filename != null) {
			launcherPath.setText(filename);
			updateLaunchConfigurationDialog();
		}
	}
	
	private void handleBrowseProject() {
		ResourceListSelectionDialog dlg = new ResourceListSelectionDialog(getShell(), 
				ResourcesPlugin.getWorkspace().getRoot(), IResource.PROJECT);
		dlg.setTitle("Select Project");
		dlg.open();
		Object[] result = dlg.getResult();
		if (result != null && result.length > 0) {
			IProject proj = (IProject)dlg.getResult()[0];
			projectName.setText(proj.getName());
			updateLaunchConfigurationDialog();
		}
	}

	@Override
	public String getName() {
		// TODO 自动生成的方法存根
		return "Main";
	}

	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
		// TODO 自动生成的方法存根
		IPreferenceStore store = LuaGamePlugin.getDefault().getPreferenceStore();
		configuration.setAttribute(LaunchConfigConstants.ATTR_LUAGAME_PATH, store.getString(PreferenceConstants.P_LAUNCHER_PATH));
		configuration.setAttribute(LaunchConfigConstants.ATTR_LUAGAME_PROJ, "");
	}

	@Override
	public void initializeFrom(ILaunchConfiguration configuration) {
		// TODO 自动生成的方法存根
		try {
			launcherPath.setText(configuration.getAttribute(LaunchConfigConstants.ATTR_LUAGAME_PATH, ""));
			projectName.setText(configuration.getAttribute(LaunchConfigConstants.ATTR_LUAGAME_PROJ, ""));
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		configuration.setAttribute(LaunchConfigConstants.ATTR_LUAGAME_PATH, launcherPath.getText());
		configuration.setAttribute(LaunchConfigConstants.ATTR_LUAGAME_PROJ, projectName.getText());
	}
}
