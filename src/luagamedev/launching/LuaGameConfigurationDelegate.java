package luagamedev.launching;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import luagamedev.LuaGamePlugin;
import luagamedev.debug.LuaGameDebugTarget;
import luagamedev.preferences.PreferenceConstants;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;

public class LuaGameConfigurationDelegate implements
		ILaunchConfigurationDelegate {

	@Override
	public void launch(ILaunchConfiguration configuration, String mode, ILaunch launch, IProgressMonitor monitor) throws CoreException {
		String launcherPath = configuration.getAttribute(LaunchConfigConstants.ATTR_LUAGAME_PATH, "");
		String projectName = configuration.getAttribute(LaunchConfigConstants.ATTR_LUAGAME_PROJ, "");
		boolean isDebug = mode.equals(ILaunchManager.DEBUG_MODE);
		
		if (launcherPath.length() == 0) {
			MessageDialog.openInformation(null, "Error", "LuaGame launcher is not defined��");
			return;
		}

		if (projectName.length() == 0) {
			MessageDialog.openInformation(null, "Error", "project name is not defined��");
			return;
		}
		
		IProject proj = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
		if (!proj.exists()) {
			MessageDialog.openInformation(null, "Error", String.format("project '%s' is not exists��", projectName));
			return;
		}
		
		List<String> commandList = new ArrayList<String>();
		
		commandList.add(launcherPath);
		commandList.add("--resource");
		commandList.add(proj.getLocation().toString());
		if (isDebug) commandList.add("--debug");
		
		try {
			ProcessBuilder procBuilder = new ProcessBuilder(commandList);
			Process proc;
			proc = procBuilder.start();
			
	        HashMap<String, String> processAttributes = new HashMap<String, String>();
	        processAttributes.put(IProcess.ATTR_PROCESS_TYPE, "luagamedev.LuaGameProcessType");
	        processAttributes.put(IProcess.ATTR_PROCESS_LABEL, String.format("%s (LuaGame)", projectName));
	        processAttributes.put(DebugPlugin.ATTR_CAPTURE_OUTPUT, "true");
	        IProcess process = DebugPlugin.newProcess(launch, proc, "LuaGame", processAttributes);
	        
	        if (isDebug) {
	        	// �ǵ���ģʽ
	        	IPreferenceStore store = LuaGamePlugin.getDefault().getPreferenceStore();
	        	int debugPort = store.getInt(PreferenceConstants.P_DEBUG_PORT);
	        	final LuaGameDebugTarget debugTarget = new LuaGameDebugTarget(launch, process, debugPort);
	        	launch.addDebugTarget(debugTarget);
	        	
	        	Thread startThread = new Thread(new Runnable() {
					@Override
					public void run() {
						debugTarget.start();
					}
				});
	        	startThread.start();
	        }
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
