package luagamedev.launching;

import luagamedev.LuaGamePlugin;
import luagamedev.preferences.PreferenceConstants;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;

public class LaunchShortcut implements ILaunchShortcut {

	@Override
	public void launch(ISelection selection, String mode) {
		// TODO 自动生成的方法存根
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection)selection;
			
			if (structuredSelection.size() == 1) {
				Object object = structuredSelection.getFirstElement();
				if (object instanceof IAdaptable) {
					IProject project = (IProject)((IAdaptable)object).getAdapter(IProject.class);
					if (project != null) {
						try {
							ILaunchConfiguration configure = createConfigure(project.getName());
							DebugUITools.launch(configure, "run");
						} catch (CoreException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	private ILaunchConfiguration createConfigure(String projectName) throws CoreException {
		ILaunchManager launchManager = DebugPlugin.getDefault().getLaunchManager();
		ILaunchConfigurationType configType = launchManager.getLaunchConfigurationType("luagamedev.launchConfigurationType");
		
		// 判断配置是否已经存在
		ILaunchConfiguration[] configs = launchManager.getLaunchConfigurations(configType);
		for (int i = 0; i < configs.length; i++) {
			String pname = configs[i].getAttribute(LaunchConfigConstants.ATTR_LUAGAME_PROJ, "");
			if (pname.equals(projectName)) {
				return configs[i];
			}
		}
		
		ILaunchConfigurationWorkingCopy configWorkCopy = configType.newInstance(null, projectName);
		IPreferenceStore store = LuaGamePlugin.getDefault().getPreferenceStore();
		
		configWorkCopy.setAttribute(LaunchConfigConstants.ATTR_LUAGAME_PATH, store.getString(PreferenceConstants.P_LAUNCHER_PATH));
		configWorkCopy.setAttribute(LaunchConfigConstants.ATTR_LUAGAME_PROJ, projectName);
		return configWorkCopy.doSave();
	}

	@Override
	public void launch(IEditorPart editor, String mode) {
		// TODO 自动生成的方法存根

	}

}
