package luagamedev.launching;

public class LaunchConfigConstants {
	public static final String ATTR_LUAGAME_PATH = "luagamedev.LuaGamePath";
	
	public static final String ATTR_LUAGAME_PROJ = "luagamedev.LuaGameProject";
}
