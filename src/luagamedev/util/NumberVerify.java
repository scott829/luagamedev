package luagamedev.util;

import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

public class NumberVerify implements Listener {

	private boolean isFloat;

	public NumberVerify(boolean isFloat) {
		this.isFloat = isFloat;
	}

	@Override
	public void handleEvent(Event e) {
		String string = e.text;
		String text = ((Text)e.widget).getText();
		char[] chars = new char[string.length()];
		boolean hasDot = text.indexOf(".") != -1;
		string.getChars(0, chars.length, chars, 0);
		int i = 0;
		if (chars.length > 0 && chars[i] == '-') i++;
		for (; i < chars.length; i++) {
			if (chars[i] == '.') {
				if (!isFloat) {
					e.doit = false;
					return;
				}
				if (hasDot) {
					e.doit = false;
					return;
				}
			} else if (!('0' <= chars[i] && chars[i] <= '9')) {
				e.doit = false;
				return;
			}
		}
	}

}
