package luagamedev.debug;

public class DebugProtocol {
	
	public static class Request {
		public String type;
		
		Request(String type) {
			this.type = type;
		}
	}
	
	public static class Response {
		public boolean succeeded;
	}
	
	public static class Event {
		public String event;
	}
	
	// 开始调试
	public static class Start extends Request {
		public Start() {
			super("start");
		}
	}
	
	// 挂起
	public static class Suspend extends Request {
		public Suspend() {
			super("suspend");
		}
	}

	// 恢复
	public static class Resume extends Request {
		public Resume() {
			super("resume");
		}
	}
	
	// 终止
	public static class Terminate extends Request {
		public Terminate() {
			super("terminate");
		}
	}
	
	// StepOver
	public static class StepOver extends Request {
		public StepOver() {
			super("step-over");
		}
	}

	// StepInto
	public static class StepInto extends Request {
		public StepInto() {
			super("step-into");
		}
	}

	// StepReturn
	public static class StepReturn extends Request {
		public StepReturn() {
			super("step-return");
		}
	}

	// 设置断点
	public static class AddBreakpoint extends Request {
		public String filename;
		public int line;
		
		public AddBreakpoint() {
			super("add-breakpoint");
		}
	}
	
	// 删除断点
	public static class RemoveBreakpoint extends Request {
		public String filename;
		public int line;
		
		public RemoveBreakpoint() {
			super("remove-breakpoint");
		}
	}
	
	// 获取调用栈
	public static class BackTrace extends Request {		
		public BackTrace() {
			super("backtrace");
		}
	}
	
	public static class BackTraceResponse extends Response {
		
		public static class Variable {
			public String name, type, value;
			public Variable[] members;
		}
		
		public static class Frame {
			public String source, name;
			public int line;
			public Variable[] variables;
		}
		
		public Frame[] frames;
	}

	// 命中断点
	public static class HitBreakpoint extends Event {
		public String filename;
		public int line;
	}
}
