package luagamedev.debug;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.core.model.IVariable;

public class LuaDebugValue extends LuaDebugElement implements IValue {
	
	private DebugProtocol.BackTraceResponse.Variable var;

	public LuaDebugValue(LuaGameDebugTarget target, DebugProtocol.BackTraceResponse.Variable var) {
		super(target);
		this.var = var;
	}

	@Override
	public String getReferenceTypeName() throws DebugException {
		return var.type;
	}

	@Override
	public String getValueString() throws DebugException {
		if (var.type.equals("string")) {
			return String.format("\"%s\"", var.value);
		} else {
			return var.value != null ? var.value : "";	
		}
	}

	@Override
	public boolean isAllocated() throws DebugException {
		return true;
	}

	@Override
	public IVariable[] getVariables() throws DebugException {
		if (var.members != null) {
			IVariable[] variables = new IVariable[var.members.length];
			for (int i = 0; i < var.members.length; i++) {
				variables[i] = new LuaDebugVariable(getLuaGameDebugTarget(), var.members[i]);
			}
			return variables;
		} else {
			return new IVariable[0];
		}
	}

	@Override
	public boolean hasVariables() throws DebugException {
		return var.members != null && var.members.length > 0;
	}
	/*
    public boolean equals(Object obj) {
        return obj instanceof LuaDebugValue && ((LuaDebugValue)obj).value.equals(value);
    }

    public int hashCode() {
        return value.hashCode();
    }*/
}
