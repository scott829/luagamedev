package luagamedev.debug;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.IThread;
import org.eclipse.debug.core.model.LineBreakpoint;

public class LuaGameBreakPoint extends LineBreakpoint implements ILuaDebuggerEventListener {
	
	private LuaGameDebugTarget target = null;
	
	public LuaGameBreakPoint() {
		
	}

	public LuaGameBreakPoint(IResource resource, int lineNumber)
			throws CoreException {
		IMarker marker = resource.createMarker("LuaGameDev.luaBreakpointMarker");
		setMarker(marker);
		setEnabled(true);
		ensureMarker().setAttribute(IMarker.LINE_NUMBER, lineNumber);
		ensureMarker().setAttribute(IBreakpoint.ID, getModelIdentifier());
		marker.setAttribute(IMarker.MESSAGE, "Line Breakpoint: " + resource.getName() + " [Line: " + lineNumber + "]");
	}
	
	private String getRelPath() {
		IResource resource = getMarker().getResource();
		IPath resourcePath = resource.getProject().getFolder("resource").getFullPath();
		return resource.getFullPath().makeRelativeTo(resourcePath).toString();
	}

	public boolean isRunToLineBreakpoint() {
		return false;
	}
	
	@Override
	public String getModelIdentifier() {
		return DebugConstants.ID_DEBUG_MODEL;
	}
	
	public void install(LuaGameDebugTarget target) throws CoreException {
		this.target = target;
		target.addEventListener(this);
		
		// 发送添加断点的请求
		DebugProtocol.AddBreakpoint addBreakpoint = new DebugProtocol.AddBreakpoint();
		addBreakpoint.filename = getRelPath();
		addBreakpoint.line = getLineNumber();
		target.sendRequest(addBreakpoint, true);
	}
	
	public void remove(LuaGameDebugTarget target) throws CoreException {
		this.target = null;
		target.removeEventListener(this);
		
		DebugProtocol.RemoveBreakpoint removeBreakpoint = new DebugProtocol.RemoveBreakpoint();
		removeBreakpoint.filename = getRelPath();
		removeBreakpoint.line = getLineNumber();
		target.sendRequest(removeBreakpoint, true);
	}

	@Override
	public void handleHitBreakpointEvent(String filename, int line) {
		try {
			if (getRelPath().equals(filename) && line == getLineNumber()) {
				// 命中断点了
				notifyThread();
			}
		} catch (CoreException e) {
		}
	}
	
	private void notifyThread() throws DebugException {
		if (target != null) {
			IThread[] threads = target.getThreads();
			if (threads.length == 1) {
				LuaDebugThread thread = (LuaDebugThread)threads[0];
				thread.suspendedBy(this);
			}
		}
	}

	@Override
	public void handleStarted() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleSuspend() {
		// TODO Auto-generated method stub
		
	}

}
