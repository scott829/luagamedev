package luagamedev.debug;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.sourcelookup.AbstractSourceLookupParticipant;

public class LuaSourceLookupParticipant extends AbstractSourceLookupParticipant {

	@Override
	public String getSourceName(Object object) throws CoreException {
		if (object instanceof LuaDebugStackFrame) {
			return ((LuaDebugStackFrame)object).getSourceName();
		}
		return null;
	}

}
