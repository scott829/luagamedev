package luagamedev.debug;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Vector;

import luagamedev.launching.LaunchConfigConstants;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IMarkerDelta;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.IBreakpointManagerListener;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IMemoryBlock;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IThread;

import com.google.gson.Gson;

public class LuaGameDebugTarget extends LuaDebugElement implements IDebugTarget, ILuaDebuggerEventListener, IBreakpointManagerListener {
	
	private boolean terminated = false;
	private ILaunch launch;
	private IProcess process;
	private Socket socketCommand, socketEvent;
	private BufferedWriter socketCommandWriter;
	private BufferedReader socketCommandReader, socketEventReader;
	
	private IThread[] threads;
	private LuaDebugThread thread;
	
	private Vector<ILuaDebuggerEventListener> eventListeners = new Vector<ILuaDebuggerEventListener>();
	private EventDispatchJob eventDispatch;
	
	class EventDispatchJob extends Job {
		
		public EventDispatchJob() {
			super("LuaGame Debugger Event Dispatch");
			setSystem(true);
		}

		protected IStatus run(IProgressMonitor monitor) {
			while (!isTerminated()) {
				try {
					StringBuffer buf = new StringBuffer();
					int c;
					String event = null;
					
					while((c = socketEventReader.read()) >= 0) {
						if (c == 0) {
							event = buf.toString();
							break;
						}
						buf.append((char)c);
					}
					
					if (event != null) {
						// 解析event
						Gson gson = new Gson();
						DebugProtocol.Event eventObject = 
								gson.fromJson(event, DebugProtocol.Event.class);
						
						if (eventObject != null && eventObject.event.equals("hit-breakpoint")) {
							DebugProtocol.HitBreakpoint hitBreakpointEvt = 
									gson.fromJson(event, DebugProtocol.HitBreakpoint.class);
							if (hitBreakpointEvt != null) {
								for (int i = 0; i < eventListeners.size(); i++)
									eventListeners.get(i).handleHitBreakpointEvent(
											hitBreakpointEvt.filename,
											hitBreakpointEvt.line);
							}
						} else if (eventObject != null && eventObject.event.equals("started")) {
							for (int i = 0; i < eventListeners.size(); i++)
								eventListeners.get(i).handleStarted();
						} else if (eventObject != null && eventObject.event.equals("suspend")) {
							for (int i = 0; i < eventListeners.size(); i++)
								eventListeners.get(i).handleSuspend();
						}
					} else {
						return Status.OK_STATUS;
					}
				} catch (IOException e) {
				}
			}
			
			return Status.OK_STATUS;
		}
		
	}
	
	public LuaGameDebugTarget(ILaunch launch, IProcess process, int debugPort) throws UnknownHostException, IOException, InterruptedException {
		super(null);
		addEventListener(this);
		this.launch = launch;
		this.process = process;
		
		// 等待一秒
		Thread.sleep(1000);
		socketCommand = new Socket("localhost", debugPort);
		socketEvent = new Socket("localhost", debugPort + 1);

		socketCommandWriter = new BufferedWriter(new OutputStreamWriter(socketCommand.getOutputStream()));
		socketCommandReader = new BufferedReader(new InputStreamReader(socketCommand.getInputStream()));
		
		socketEventReader = new BufferedReader(new InputStreamReader(socketEvent.getInputStream()));

		// 创建主线程对象
		thread = new LuaDebugThread(this);
		threads = new IThread[] {thread};
		
		// 接受调试器事件
		eventDispatch = new EventDispatchJob();
		eventDispatch.schedule();

		// 安装断点
		installDeferredBreakpoints();
		getBreakpointManager().addBreakpointListener(this);
		getBreakpointManager().addBreakpointManagerListener(this);
	}
	
	public void addEventListener(ILuaDebuggerEventListener listener) {
		if (!eventListeners.contains(listener)) {
			eventListeners.add(listener);
		}
	}
	
	public void removeEventListener(ILuaDebuggerEventListener listener) {
		eventListeners.remove(listener);
	}
	
	public void start() {
		sendRequest(new DebugProtocol.Start(), false);
	}
	
	public String sendRequest(DebugProtocol.Request protocol, boolean hasReply) {
		synchronized(socketCommandReader) {
			Gson gson = new Gson();
			String json = gson.toJson(protocol);
			
			try {
				socketCommandWriter.write(json);
				socketCommandWriter.write(0);
				socketCommandWriter.flush();
				
				if (hasReply) {
					StringBuffer buf = new StringBuffer();
					int c;
					
					while((c = socketCommandReader.read()) >= 0) {
						if (c == 0)
							return buf.toString();
						buf.append((char)c);
					}
				}
				
				return null;
			}catch(IOException e) {
				return null;
			}
		}
	}

	private void installDeferredBreakpoints() {
		IBreakpoint[] breakpoints = getBreakpointManager().
				getBreakpoints(DebugConstants.ID_DEBUG_MODEL);
		for (int i = 0; i < breakpoints.length; i++) {
			breakpointAdded(breakpoints[i]);
		}
	}

	@Override
	public IDebugTarget getDebugTarget() {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public ILaunch getLaunch() {
		// TODO Auto-generated method stub
		return launch;
	}

	@Override
	public Object getAdapter(Class adapter) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean canTerminate() {
		return getProcess().canTerminate();
	}

	@Override
	public boolean isTerminated() {
		boolean processTerminated = getProcess().isTerminated();
		return terminated || processTerminated;
	}

	@Override
	public void terminate() throws DebugException {
		getThread().terminate();
	}

	@Override
	public boolean canResume() {
		return !isTerminated() && isSuspended();
	}

	@Override
	public boolean canSuspend() {
		return !isTerminated() && !isSuspended();
	}

	@Override
	public boolean isSuspended() {
		return !isTerminated() && getThread().isSuspended();
	}

	@Override
	public void resume() throws DebugException {
		getThread().resume();
	}

	@Override
	public void suspend() throws DebugException {
		getThread().suspend();
	}

	@Override
	public void breakpointAdded(IBreakpoint breakpoint) {
		if (supportsBreakpoint(breakpoint)) {
			try {
				if (breakpoint.isEnabled() && breakpoint.isRegistered()) {
					LuaGameBreakPoint luagameBreakpoint = (LuaGameBreakPoint)breakpoint;
					luagameBreakpoint.install(this);
				}
			}catch(CoreException e) {
			}
		}
	}

	@Override
	public void breakpointRemoved(IBreakpoint breakpoint, IMarkerDelta delta) {
		if (supportsBreakpoint(breakpoint)) {
			try {
				LuaGameBreakPoint luagameBreakpoint = (LuaGameBreakPoint)breakpoint;
				luagameBreakpoint.remove(this);
			}catch(CoreException e) {
			}
		}
	}

	@Override
	public void breakpointChanged(IBreakpoint breakpoint, IMarkerDelta delta) {
		if (supportsBreakpoint(breakpoint)) {
			try {
				if (breakpoint.isEnabled()) breakpointAdded(breakpoint);
				else breakpointRemoved(breakpoint, null);
			}catch(CoreException e) {
			}
		}
	}

	@Override
	public boolean canDisconnect() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void disconnect() throws DebugException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isDisconnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean supportsStorageRetrieval() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public IMemoryBlock getMemoryBlock(long startAddress, long length)
			throws DebugException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IProcess getProcess() {
		// TODO Auto-generated method stub
		return process;
	}

	@Override
	public IThread[] getThreads() throws DebugException {
		// TODO Auto-generated method stub
		return threads;
	}

	@Override
	public boolean hasThreads() throws DebugException {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String getName() throws DebugException {
		// TODO Auto-generated method stub
		return "LuaGame";
	}

	@Override
	public boolean supportsBreakpoint(IBreakpoint breakpoint) {
		if (breakpoint.getModelIdentifier().equals(DebugConstants.ID_DEBUG_MODEL)) {
			try {
				String projectName = getLaunch().getLaunchConfiguration()
						.getAttribute(LaunchConfigConstants.ATTR_LUAGAME_PROJ, (String)null);
				if (projectName != null) {
					IMarker marker = breakpoint.getMarker();
					if (marker != null) {
						return (marker.getResource().getProject().getName().equals(projectName));
					}
				}
			} catch (CoreException e) {
			}
		}
		return false;
	}
	
	public synchronized LuaDebugThread getThread() {
		return thread;
	}

	@Override
	public void handleStarted() {
		fireCreationEvent();
	}

	@Override
	public void handleHitBreakpointEvent(String filename, int line) {
	}

	@Override
	public void handleSuspend() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void breakpointManagerEnablementChanged(boolean enabled) {
		IBreakpoint[] breakpoints = getBreakpointManager().getBreakpoints(
				getModelIdentifier());
		for (int i = 0; i < breakpoints.length; i++) {
			if (enabled) {
				breakpointAdded(breakpoints[i]);
			} else {
				breakpointRemoved(breakpoints[i], null);
			}
		}
	}
}
