package luagamedev.debug;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IRegisterGroup;
import org.eclipse.debug.core.model.IStackFrame;
import org.eclipse.debug.core.model.IThread;
import org.eclipse.debug.core.model.IVariable;

public class LuaDebugStackFrame extends LuaDebugElement implements IStackFrame {
	
	private LuaDebugThread thread;
	private int id;
	private DebugProtocol.BackTraceResponse.Frame frame;
	private IVariable[] variables;
	
	LuaDebugStackFrame(LuaDebugThread thread, DebugProtocol.BackTraceResponse.Frame frame, int id) {
		super(thread.getLuaGameDebugTarget());
		this.frame = frame;
		this.thread = thread;
		this.id = id;
		
		if (frame.variables != null) {
			variables = new IVariable[frame.variables.length];
			for (int i = 0; i < frame.variables.length; i++) {
				variables[i] = new LuaDebugVariable(thread.getLuaGameDebugTarget(), frame.variables[i]);
			}
		} else {
			variables = new IVariable[0];
		}
	}
	
	public String getSourceName() {
		return frame.source;
	}

	@Override
	public boolean canStepInto() {
		return thread.canStepInto();
	}

	@Override
	public boolean canStepOver() {
		return thread.canStepOver();
	}

	@Override
	public boolean canStepReturn() {
		return getThread().canStepReturn();
	}

	@Override
	public boolean isStepping() {
		return getThread().isStepping();
	}

	@Override
	public void stepInto() throws DebugException {
		getThread().stepInto();
	}

	@Override
	public void stepOver() throws DebugException {
		getThread().stepOver();
	}

	@Override
	public void stepReturn() throws DebugException {
		getThread().stepReturn();
	}

	@Override
	public boolean canResume() {
		return getThread().canResume();
	}

	@Override
	public boolean canSuspend() {
		return getThread().canSuspend();
	}

	@Override
	public boolean isSuspended() {
		return getThread().isSuspended();
	}

	@Override
	public void resume() throws DebugException {
		getThread().resume();
	}

	@Override
	public void suspend() throws DebugException {
		getThread().suspend();
	}

	@Override
	public boolean canTerminate() {
		return getThread().canTerminate();
	}

	@Override
	public boolean isTerminated() {
		return getThread().isTerminated();
	}

	@Override
	public void terminate() throws DebugException {
		getThread().terminate();
	}

	@Override
	public IThread getThread() {
		return thread;
	}

	@Override
	public IVariable[] getVariables() throws DebugException {
		return variables;
	}

	@Override
	public boolean hasVariables() throws DebugException {
		return true;
	}

	@Override
	public int getLineNumber() throws DebugException {
		// TODO Auto-generated method stub
		return frame.line;
	}

	@Override
	public int getCharStart() throws DebugException {
		return -1;
	}

	@Override
	public int getCharEnd() throws DebugException {
		return -1;
	}

	@Override
	public String getName() throws DebugException {
		return String.format("%s (%s:%d)", 
				frame.name != null ? frame.name : "(anonymous)", 
				frame.source, frame.line);
	}

	@Override
	public IRegisterGroup[] getRegisterGroups() throws DebugException {
		return null;
	}

	@Override
	public boolean hasRegisterGroups() throws DebugException {
		return false;
	}

	public boolean equals(Object obj) {
		if (obj instanceof LuaDebugStackFrame) {
			LuaDebugStackFrame sf = (LuaDebugStackFrame)obj;
			return sf.getThread().equals(getThread()) && 
				sf.getSourceName().equals(getSourceName()) &&
				sf.id == id && sf.frame.line== frame.line;
		}
		return false;
	}
	
	public int hashCode() {
		return getSourceName().hashCode() + id;
	}
}
