package luagamedev.debug;

import luagamedev.launching.LaunchConfigConstants;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.sourcelookup.ISourceContainer;
import org.eclipse.debug.core.sourcelookup.ISourcePathComputerDelegate;
import org.eclipse.debug.core.sourcelookup.containers.FolderSourceContainer;
import org.eclipse.debug.core.sourcelookup.containers.WorkspaceSourceContainer;

public class LuaSourcePathComputerDelegate implements
		ISourcePathComputerDelegate {

	@Override
	public ISourceContainer[] computeSourceContainers(
			ILaunchConfiguration configuration, IProgressMonitor monitor)
			throws CoreException {
		String projectName = configuration.getAttribute(LaunchConfigConstants.ATTR_LUAGAME_PROJ, (String)null);
		ISourceContainer sourceContainer = null;
		IProject proj = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
		
		if (proj != null) {
			sourceContainer = new FolderSourceContainer(proj.getFolder("resource"), false);
		}
		
		if (sourceContainer == null) {
	          sourceContainer = new WorkspaceSourceContainer();
		}
		return new ISourceContainer[]{sourceContainer};
	}

}
