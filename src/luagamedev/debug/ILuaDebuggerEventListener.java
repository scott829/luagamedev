package luagamedev.debug;

public interface ILuaDebuggerEventListener {
	void handleStarted();
	
	void handleHitBreakpointEvent(String filename, int line);
	
	void handleSuspend();
}
