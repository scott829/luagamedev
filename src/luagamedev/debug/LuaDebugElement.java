package luagamedev.debug;

import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IBreakpointManager;
import org.eclipse.debug.core.model.DebugElement;
import org.eclipse.debug.core.model.IDebugTarget;

public class LuaDebugElement extends DebugElement {
	
	public LuaDebugElement(IDebugTarget target) {
		super(target);
	}

	@Override
	public String getModelIdentifier() {
		return DebugConstants.ID_DEBUG_MODEL;
	}
	
	protected LuaGameDebugTarget getLuaGameDebugTarget() {
	    return (LuaGameDebugTarget)getDebugTarget();
	}
	
    protected IBreakpointManager getBreakpointManager() {
        return DebugPlugin.getDefault().getBreakpointManager();
    }
    
    public String sendRequest(DebugProtocol.Request protocol, boolean hasReply) {
    	return getLuaGameDebugTarget().sendRequest(protocol, hasReply);
    }
}
