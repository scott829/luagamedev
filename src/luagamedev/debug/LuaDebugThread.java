package luagamedev.debug;

import java.util.Vector;

import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.IStackFrame;
import org.eclipse.debug.core.model.IThread;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class LuaDebugThread extends LuaDebugElement implements IThread, ILuaDebuggerEventListener {
	
	private IBreakpoint breakpoint = null;
	private boolean stepping = false;
	private boolean suspended = false;
	
	LuaDebugThread(LuaGameDebugTarget target) {
		super(target);
		target.addEventListener(this);
	}

	@Override
	public boolean canResume() {
		return isSuspended();
	}

	@Override
	public boolean canSuspend() {
		return !isSuspended();
	}

	@Override
	public boolean isSuspended() {
		return suspended && !isTerminated();
	}

	@Override
	public void resume() throws DebugException {
		suspended = false;
		breakpoint = null;
		sendRequest(new DebugProtocol.Resume(), true);
		fireResumeEvent(DebugEvent.CLIENT_REQUEST);
	}

	@Override
	public void suspend() throws DebugException {
		sendRequest(new DebugProtocol.Suspend(), true);
	}

	@Override
	public boolean canStepInto() {
		return isSuspended();
	}

	@Override
	public boolean canStepOver() {
		return isSuspended();
	}

	@Override
	public boolean canStepReturn() {
		return isSuspended();
	}

	@Override
	public boolean isStepping() {
		return stepping;
	}

	@Override
	public void stepInto() throws DebugException {
		suspended = false;
		stepping = true;
		fireResumeEvent(DebugEvent.STEP_INTO);
		sendRequest(new DebugProtocol.StepInto(), true);
	}

	@Override
	public void stepOver() throws DebugException {
		suspended = false;
		stepping = true;
		fireResumeEvent(DebugEvent.STEP_OVER);
		sendRequest(new DebugProtocol.StepOver(), true);
	}

	@Override
	public void stepReturn() throws DebugException {
		suspended = false;
		stepping = true;
		fireResumeEvent(DebugEvent.STEP_RETURN);
		sendRequest(new DebugProtocol.StepReturn(), true);
	}

	@Override
	public boolean canTerminate() {
		return !isTerminated();
	}

	@Override
	public boolean isTerminated() {
		return getDebugTarget().isTerminated();
	}

	@Override
	public void terminate() throws DebugException {
		sendRequest(new DebugProtocol.Terminate(), false);
		fireTerminateEvent();
	}

	@Override
	public IStackFrame[] getStackFrames() throws DebugException {
		if (isSuspended()) {
			String response = sendRequest(new DebugProtocol.BackTrace(), true);
			if (response != null) {
				Gson gson = new Gson();
				try {
					DebugProtocol.BackTraceResponse btr = gson.fromJson(response, DebugProtocol.BackTraceResponse.class);
					
					if (btr != null && btr.succeeded) {
						Vector<IStackFrame> frames = new Vector<IStackFrame>();
						
						for (int i = 0; i < btr.frames.length; i++) {
							DebugProtocol.BackTraceResponse.Frame frame = btr.frames[i];
							LuaDebugStackFrame stackFrame = new LuaDebugStackFrame(this, frame, i);
							frames.add(stackFrame);
						}
						
						return (IStackFrame[])frames.toArray(new IStackFrame[frames.size()]);
					}
				} catch(JsonSyntaxException e) {
					e.printStackTrace();
				}
			}	
		}
		
		return new IStackFrame[0];
	}

	@Override
	public boolean hasStackFrames() throws DebugException {
		return isSuspended();
	}

	@Override
	public int getPriority() throws DebugException {
		return 0;
	}

	@Override
	public IStackFrame getTopStackFrame() throws DebugException {
		IStackFrame[] frames = getStackFrames();
		if (frames.length > 0) {
			return frames[0];
		}
		return null;
	}

	@Override
	public String getName() throws DebugException {
		return "Main Thread";
	}

	@Override
	public IBreakpoint[] getBreakpoints() {
		if (breakpoint == null) {
			return new IBreakpoint[0];
		}
		return new IBreakpoint[]{breakpoint};
	}
	
	public void suspendedBy(IBreakpoint breakpoint) {
		this.breakpoint = breakpoint;
		suspended = true;
		fireSuspendEvent(DebugEvent.BREAKPOINT);
	}

	@Override
	public void handleHitBreakpointEvent(String filename, int line) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleStarted() {
		fireCreationEvent();
	}

	@Override
	public void handleSuspend() {
		suspended = true;
		
		if (stepping) {
			stepping = false;
			fireSuspendEvent(DebugEvent.STEP_END);			
		} else {
			fireSuspendEvent(DebugEvent.CLIENT_REQUEST);
		}
	}
}
