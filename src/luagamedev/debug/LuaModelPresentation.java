package luagamedev.debug;

import luagamedev.launching.LaunchConfigConstants;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.ILineBreakpoint;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.ui.IDebugModelPresentation;
import org.eclipse.debug.ui.IValueDetailListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.part.FileEditorInput;

public class LuaModelPresentation extends LabelProvider implements
		IDebugModelPresentation {

	@Override
	public IEditorInput getEditorInput(Object element) {
		if (element instanceof IFile) {
			return new FileEditorInput((IFile)element);
		}
		if (element instanceof ILineBreakpoint) {
			return new FileEditorInput((IFile)((ILineBreakpoint)element).getMarker().getResource());
		}
		return null;
	}

	@Override
	public String getEditorId(IEditorInput input, Object element) {
		if (element instanceof IFile || element instanceof ILineBreakpoint) {
			return "luagamedev.LUAEditor";
		}
		return null;
	}

	@Override
	public void setAttribute(String attribute, Object value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void computeDetail(IValue value, IValueDetailListener listener) {
		// TODO Auto-generated method stub

	}
	
	public String getText(Object element) {
		try {
			if (element instanceof LuaGameDebugTarget) {
				LuaGameDebugTarget target = (LuaGameDebugTarget)element;
				return getTargetLabel(target);
			} else if (element instanceof LuaDebugThread) {
				LuaDebugThread thread = (LuaDebugThread)element;
				return getThreadLabel(thread);
			} else if (element instanceof LuaDebugStackFrame) {
				LuaDebugStackFrame stackFrame = (LuaDebugStackFrame)element;
				return getStackFrameLabel(stackFrame);
			} else {
				return element.toString();
			}
		} catch (Exception e) {
			return element.toString();
		}
	}

	private String getTargetLabel(LuaGameDebugTarget target)
			throws CoreException {
		String projectName = target
				.getLaunch()
				.getLaunchConfiguration()
				.getAttribute(LaunchConfigConstants.ATTR_LUAGAME_PROJ,
						(String) null);
		String label = "";
		if (target.isTerminated()) {
			label = "<terminated>";
		}
		return label + projectName;
	}

	private String getThreadLabel(LuaDebugThread thread) throws DebugException {
		String label = thread.getName();
		if (thread.isStepping()) {
			label += " (stepping)";
		} else if (thread.isSuspended()) {
			IBreakpoint[] breakpoints = thread.getBreakpoints();
			if (breakpoints.length == 0) {
				label += " (suspended)";
			} else {
				IBreakpoint breakpoint = breakpoints[0];
				if (breakpoint instanceof LuaGameBreakPoint) {
					LuaGameBreakPoint lgBreakpoint = (LuaGameBreakPoint) breakpoint;
					if (lgBreakpoint.isRunToLineBreakpoint()) {
						label += " (run to line)";
					} else {
						label += " (suspended at line breakpoint)";
					}
				}
			}
		} else if (thread.isTerminated()) {
			label = "<terminated> " + label;
		}
		return label;
	}
	
	private String getStackFrameLabel(LuaDebugStackFrame stackFrame) throws DebugException {
		return stackFrame.getName();
	}

}
