package luagamedev.debug;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.core.model.IVariable;

public class LuaDebugVariable extends LuaDebugElement implements IVariable {
	
	private DebugProtocol.BackTraceResponse.Variable var;

	public LuaDebugVariable(LuaGameDebugTarget target, DebugProtocol.BackTraceResponse.Variable var) {
		super(target);
		this.var = var;
	}

	@Override
	public void setValue(String expression) throws DebugException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setValue(IValue value) throws DebugException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean supportsValueModification() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyValue(String expression) throws DebugException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyValue(IValue value) throws DebugException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public IValue getValue() throws DebugException {
		return new LuaDebugValue(getLuaGameDebugTarget(), var);
	}

	@Override
	public String getName() throws DebugException {
		return var.name;
	}

	@Override
	public String getReferenceTypeName() throws DebugException {
		return null;
	}

	@Override
	public boolean hasValueChanged() throws DebugException {
		return false;
	}

}
