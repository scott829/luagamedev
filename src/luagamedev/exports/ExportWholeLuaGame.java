package luagamedev.exports;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;

public class ExportWholeLuaGame extends Wizard implements IExportWizard {
	
	private ExportWholePage pathpage;
	private ISelection selection;

	public ExportWholeLuaGame() {
		setWindowTitle("Export whole LuaGame");
		setNeedsProgressMonitor(true);
	}
	
	public void addPages() {
		pathpage = new ExportWholePage(selection);
		addPage(pathpage);
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}

	@Override
	public boolean performFinish() {
		// TODO Auto-generated method stub
		return false;
	}

}
