package luagamedev.exports;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

public class ExportWholePage extends WizardPage {
	
	Text directoryText, filenameText;

	public ExportWholePage(ISelection selection) {
		super("pathPage");
		setTitle("Export whole LuaGame");
		setDescription("Please select destination:");
		setPageComplete(false);
	}
	
	private void createDestinationTab(TabFolder parent) {
		TabItem destinationTab = new TabItem(parent, SWT.NONE);
		destinationTab.setText("Destination");
		Composite control = new Composite(parent, SWT.NULL);
		Button button;
		GridLayout slayout;
		
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		layout.verticalSpacing = 9;
		control.setLayout(layout);
		
		// 导出为目录
		button = new Button(control, SWT.RADIO);
		button.setText("Directory:");
		button.setSelection(true);
		Composite selectDirComposite = new Composite(control, SWT.NULL);
		selectDirComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		slayout = new GridLayout();
		slayout.numColumns = 2;
		selectDirComposite.setLayout(slayout);
		directoryText = new Text(selectDirComposite, SWT.BORDER | SWT.SINGLE);
		directoryText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		Button selectDirBtn = new Button(selectDirComposite, SWT.PUSH);
		selectDirBtn.setText("&Browse...");
		
		// 导出为压缩包
		button = new Button(control, SWT.RADIO);
		button.setText("Archive File:");
		Composite selectFileComposite = new Composite(control, SWT.NULL);
		selectFileComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		slayout = new GridLayout();
		slayout.numColumns = 2;
		selectFileComposite.setLayout(slayout);
		filenameText = new Text(selectFileComposite, SWT.BORDER | SWT.SINGLE);
		filenameText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		Button selectFileBtn = new Button(selectFileComposite, SWT.PUSH);
		selectFileBtn.setText("&Browse...");

		destinationTab.setControl(control);
	}
	
	private void createOptionsTab(TabFolder parent) {
		TabItem optionsTab = new TabItem(parent, SWT.NONE);
		optionsTab.setText("Options");
	}
	
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);

		container.setLayout(new FillLayout());
		
		TabFolder tab = new TabFolder(container, SWT.TOP);
		createDestinationTab(tab);
		createOptionsTab(tab);
		setControl(container);
	}

}
